package utilidades;

import java.io.*;

public class Entrada {
	static String inicializar() {
		String buzon = "";
		InputStreamReader flujo = new InputStreamReader(System.in);
		BufferedReader teclado = new BufferedReader(flujo);
		try {
			buzon = teclado.readLine();
		} catch (Exception e) {
			System.out.append("Entrada incorrecta)");
		}
		return buzon;
	}

	public static int entero() {
		int valor = Integer.parseInt(inicializar());
		return valor;
	}

	public static double real() {
		double valor = Double.parseDouble(inicializar());
		return valor;
	}

	public static String cadena() {
		String valor = inicializar();
		return valor;
	}

	public static char caracter() {
		String valor = inicializar();
		return valor.charAt(0);
	}
}

/*
private static Connection conectarBBDD() throws Exception {
		java.sql.Connection con = null;

		ArrayList<String> lineas = new ArrayList<>();
		String linea;
		File f = new File("CFG2.INI");
		BufferedReader br = new BufferedReader(new FileReader(f));
		while ((linea = br.readLine()) != null) {
			lineas.add(linea);
		}
		br.close();
		
		String IP = lineas.get(0);
		String usu = lineas.get(1);
		String pass = lineas.get(2);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://"+IP+"/", usu, pass);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion",
					JOptionPane.ERROR_MESSAGE);
		}
		return con;
	}


private static void listadoNormal(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO NORMAL:");
		for (String i : al) {
			System.out.println(i);
		}
		
	}
	
private static void borrarPersona(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="DELETE FROM persona WHERE nombre='Gonzalo'";
		java.sql.PreparedStatement pst = conexion.prepareStatement(selec);
		pst.executeUpdate();
		
		pst.close();
	}

private static void darAltaPersona(java.sql.Connection con) throws Exception { //MAAAAAL
		java.sql.Connection conexion = con;		
		
		String comprobar="SELECT * FROM persona";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(comprobar);
		
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		for (int i = 0; i < al.size(); i++) {
			if (al.get(i).toString().equals("Gonzalo")) {
				java.sql.PreparedStatement pst;
				String update="UPDATE persona SET nombre = 'Gonzalo' WHERE nombre = 'Gonzalo'";		
				pst = conexion.prepareStatement(update);
				pst.executeUpdate();
				pst.close();
			} else {
				java.sql.PreparedStatement pst;
				String insert="INSERT INTO persona (nombre,CP,pais,email) VALUES (\"Gonzalo\",\"28430\",\"España\",\"g.dlt@gmail.com\")";		
				pst = conexion.prepareStatement(insert);
				pst.executeUpdate();
				pst.close();
			}
		}
		
		rs.close();
		st.close();
	}


private static void listarTablaBBDD(Connection con) throws SQLException {
		
		ResultSet rs = con.getMetaData().getTables(bbdd, null, "%", null); //Introducir bbdd, posiblemente desde parametro

		while (rs.next()) {
			String catalogo = rs.getString(1);
			String tabla = rs.getString(3);
		    System.out.println("TABLA=" + catalogo + "." + tabla);
		}
	}

	private static void listarBBDD(Connection con) throws SQLException {
		ResultSet rs = con.getMetaData().getCatalogs();

		while (rs.next()) {
		    System.out.println("TABLE_CAT = " + rs.getString("TABLE_CAT") );
		}
	}
	
*/