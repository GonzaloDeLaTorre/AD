package miniBanco;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.Font;

public class InterfazUsuario extends JFrame {

	private JPanel contentPane;

	GestorBD bd = null;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 * 
	 * @throws SQLException
	 */

	public static void main(String[] args) throws SQLException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuario frame = new InterfazUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		GestorBD bd = new GestorBD();

	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuario() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 481);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton Boton_Alta = new JButton("ALTA");
		Boton_Alta.setMnemonic('a');
		Boton_Alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				crearAlta();
			}
		});
		Boton_Alta.setBounds(63, 60, 117, 25);
		contentPane.add(Boton_Alta);

		JButton Boton_Ingreso = new JButton("INGRESO");
		Boton_Ingreso.setMnemonic('i');
		Boton_Ingreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					realizarIngreso();
				} catch (SQLException e) {
					utilidades.Utilidades.notificarError(null, "Ingreso", e, "Error al realizar el ingreso: \n");
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Boton_Ingreso.setBounds(245, 60, 117, 25);
		contentPane.add(Boton_Ingreso);

		JButton Boton_Reintegro = new JButton("REINTEGRO");
		Boton_Reintegro.setMnemonic('r');
		Boton_Reintegro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					realizarReintegro();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

		});
		Boton_Reintegro.setBounds(63, 155, 117, 25);
		contentPane.add(Boton_Reintegro);

		/**
		 * BOTON EXTRACTO
		 */
		JButton Boton_Extracto = new JButton("EXTRACTO");
		Boton_Extracto.setMnemonic('e');
		Boton_Extracto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					extracto();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		Boton_Extracto.setBounds(245, 155, 117, 25);
		contentPane.add(Boton_Extracto);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 210, 398, 233);
		contentPane.add(scrollPane);

		textArea = new JTextArea();
		textArea.setDisabledTextColor(Color.BLUE);
		textArea.setSelectedTextColor(Color.BLACK);
		textArea.setSelectionColor(Color.BLACK);
		textArea.setFont(new Font("Serif", Font.BOLD, 12));
		textArea.setForeground(Color.RED);
		scrollPane.setViewportView(textArea);
		textArea.setEnabled(false);
		textArea.setEditable(false);
	}

	/**
	 * Metodo para realizar reintegros(extraer dinero de la cuenta).
	 * 
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	protected void realizarReintegro() throws NumberFormatException, Exception { // REINTEGRO
		bd = new GestorBD();
		// Consultar ID de la cuenta
		String id = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta",
				JOptionPane.DEFAULT_OPTION);
		// Si pulsamos en el boton cancelar en id
		if (id == null) {
			JOptionPane.showMessageDialog(null, "Operación cancelada.");
			// si pulsamos en el boton aceptar
		} else {
			boolean cond = condicionId(id);
			if (cond == false) {
				// Buscar ID
				boolean ex = bd.consultarExistencia(Integer.valueOf(id));
				if (ex != false) {
					// Si existe el ID
					int saldo = bd.consultarSaldo(Integer.valueOf(id));
					String j2 = JOptionPane.showInputDialog(this,
							"ID: " + id + "\nSaldo: " + saldo + "\nImporte del reintegro: ", "Consulta de cuenta",
							JOptionPane.DEFAULT_OPTION);
					// Si pulsamos el boton cancelar en saldo
					if (j2 == null) {
						JOptionPane.showMessageDialog(null, "Operación cancelada.");
						// Si pulsamos el boton aceptar
					} else {
						cond = condicionImporte(j2);
						if (cond == false) {
							int res = Integer.valueOf(j2);
							if (!(res > 0 && res < saldo)) { // COMPROBAR
								JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.",
										"Error", JOptionPane.ERROR_MESSAGE);
							} else {
								bd.reintegro(Integer.valueOf(id), res);
							}
						}
					}

				} else {
					JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}

		}

	}

	/**
	 * Metodo para realizar ingreso en una cuenta, introduciendo el id de la cuenta
	 * y el valor del ingreso en positivo.
	 * 
	 * @throws Exception
	 */
	protected void realizarIngreso() throws Exception { // INGRESO
		bd = new GestorBD();
		// Consultar ID de la cuenta
		// j es el id de la cuenta

		String j = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta",
				JOptionPane.DEFAULT_OPTION);

		// Si pulsamos el boton cancelar en el id, mensaje informativo
		if (j == null) {
			JOptionPane.showMessageDialog(null, "Operación cancelada");
			// Si hemos pulsado en aceptar
		} else {

			int id = Integer.parseInt(j);
			boolean cond = condicionId(j);
			if (cond == false) {
				// Buscar ID
				// Si existe el ID
				boolean ex = bd.consultarExistencia(id);
				if (ex != false) {
					int saldo = bd.consultarSaldo(Integer.valueOf(id));
					String j2 = JOptionPane.showInputDialog(this,
							"ID: " + id + "\nSaldo: " + saldo + "\nImporte del ingreso: ", "Consulta de cuenta",
							JOptionPane.DEFAULT_OPTION);

					// Si hemos pulsado cancelar en el valor del ingreso
					if (j2 == null) {
						JOptionPane.showMessageDialog(null, "Operación cancelada");
						// Si hemos pulsado en aceptar
					} else {

						cond = condicionImporte(j2);
						if (cond == false) {
							int res = Integer.valueOf(j2); // importe a ingresar

							if (!(res > 0)) {
								JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.",
										"Error", JOptionPane.ERROR_MESSAGE);
							} else {
								// Incrementar saldo de la cuenta
								try {
									bd.ingreso(id, res);
								} catch (Exception e) {
									utilidades.Utilidades.notificarError(null, "Cuenta", e,
											"La cuenta con id: " + id + " no existe!\n");
									e.printStackTrace();
								}
							}
						}
					}
				} else {
					JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
				}

			}

		}

	}

	/**
	 * Da de alta a una cuenta nueva
	 */
	protected void crearAlta() {

		// Crear alta
		int saldo = 0;
		Cuenta c = new Cuenta(saldo);
		try {
			bd = new GestorBD();
			bd.crearCuenta(c);
		} catch (Exception e) {
			utilidades.Utilidades.notificarError(null, "Error", e, "Error al crear cuenta. ");

		}

	}

	/**
	 * Comprueba que el importe introducido sea correcto.
	 * 
	 * @param j2
	 *            es el importe introducido por el usuario
	 * @return true si el importe es correcto
	 */
	private boolean condicionImporte(String j2) {
		boolean res = false;
		if (!j2.matches("[0-9]{1,11}")) {
			JOptionPane.showMessageDialog(this,
					"El importe debe tener una longitud mínima de 1 caracter numérico y máxima de 11 caracteres numéricos.",
					"Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}

	/**
	 * Comprueba que el id introducido sea correcto.
	 * 
	 * @param j
	 *            es el id introducido por el usuario
	 * @return true si el id es correcto
	 */
	private boolean condicionId(String j) {
		boolean res = false;
		if (!j.matches("[0-9]{1,11}")) {
			JOptionPane.showMessageDialog(this,
					"El ID debe tener una longitud mínima de 1 caracter numérico y máxima de 11 caracteres numéricos.",
					"Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}

	/**
	 * Controla que el formato de fecha introducido es correcto yyyy-MM-dd
	 * 
	 * @param fechaDesde
	 *            fecha introducida por el usuario
	 * @return true si la fecha es correcta
	 */
	private boolean condicionFecha(String fechaDesde) {
		boolean res = false;
		if (fechaDesde.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
			res = true;
		}
		return res;
	}

	/**
	 * Metodo para sacar a un jTextArea un extracto de la cuenta. LLama al metodo
	 * consultarExtracto que recibe como parametro el id, fechaDesde y fechaHasta
	 * 
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	protected void extracto() throws NumberFormatException, Exception {
		bd = new GestorBD();
		String id = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta",
				JOptionPane.DEFAULT_OPTION);
		// Si pulsamos cancelar en el id
		if (id == null) {
			JOptionPane.showMessageDialog(null, "Operación cancelada");
			// si pulsamos aceptar
		} else {
			boolean cond = condicionId(id);
			if (cond == false) {
				// Buscar ID
				boolean ex = bd.consultarExistencia(Integer.valueOf(id));
				if (ex != false) {
					// Si existe el ID
					String fechaDesde = JOptionPane.showInputDialog(this, "ID: " + id + "\nFecha desde: ",
							"Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
					// si pulsamos en cancelar en fecha desde
					if (fechaDesde == null) {
						JOptionPane.showMessageDialog(null, "Operación cancelada");
						// si pulsamos en aceptar
					} else {
						cond = condicionFecha(fechaDesde);
						if (cond == true) {
							String fechaHasta = JOptionPane.showInputDialog(this,
									"ID: " + id + "\nFecha desde: " + fechaDesde + "\nFecha hasta: ",
									"Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
							// Si pulsamos en cancelar en fecha hasta
							if (fechaHasta == null) {
								JOptionPane.showMessageDialog(null, "Operación cancelada");
								// si pulsamos en aceptar
							} else {
								cond = condicionFecha(fechaHasta);
								if (cond == true) {
									JOptionPane.showMessageDialog(this,
											"ID: " + id + "\nFecha desde: " + fechaDesde + "\nFecha hasta: "
													+ fechaHasta,
											"Consulta de cuenta", JOptionPane.INFORMATION_MESSAGE);
									// ArrayList en el que guardamos todos los movimientos comprendidos entre las
									// dos fechas introducidas
									ArrayList<Movimiento> al = new ArrayList<Movimiento>();
									// llamamos al metodo consultar extracto
									al = bd.consultarExtracto(id, fechaDesde, fechaHasta);
									textArea.setText("");// Borramos el contenido que habia en el textArea
									// Mostramos nombre de las columnas
									textArea.setText(String.format("%2S%9S%33S%13S", "Id", "Fecha", "Importe",
											"Id_cuenta" + "\n"));
									// Mostramos todos los movimientos de la cuenta
									for (Movimiento movimiento : al) {

										textArea.setText(textArea.getText() + movimiento.toString() + "\n");

									}

								} else {
									JOptionPane.showMessageDialog(this,
											"Fecha incorrecta.\nFormato: yyyy-MM-dd\nEjemplo:2019-12-25", "Error",
											JOptionPane.ERROR_MESSAGE);
								}

							}

						} else {
							JOptionPane.showMessageDialog(this,
									"Fecha incorrecta.\nFormato: yyyy-MM-dd\nEjemplo:2019-12-25", "Error",
									JOptionPane.ERROR_MESSAGE);
						}

					}

				} else {
					JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}

		}

	}
}
