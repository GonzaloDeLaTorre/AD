package persistencia;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextField;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Interfaz_Clientes extends JFrame {

	private JPanel contentPane;
	private static JTextField textField_Email;
	private JTextField textField_Nombre;
	private JTextField textField_Apellidos;
	private JTable table;
	private DefaultTableModel tableModel;
	private JButton button_Guardar;
	private JButton button_Borrar;
	private JButton button_Cancelar;
	
	Persistencia per = PrincipalGUI.per;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Clientes frame = new Interfaz_Clientes();
					frame.setVisible(true);
					textField_Email.grabFocus();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interfaz_Clientes() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 427, 442);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel contentPane_1 = new JPanel();
		contentPane_1.setLayout(null);
		contentPane_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane_1.setBounds(0, 0, 564, 403);
		contentPane.add(contentPane_1);
		
		textField_Email = new JTextField();
		textField_Email.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textField_Nombre.grabFocus();
			}
		});
		textField_Email.setColumns(10);
		textField_Email.setBounds(101, 25, 114, 19);
		contentPane_1.add(textField_Email);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(12, 27, 38, 15);
		contentPane_1.add(lblEmail);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(12, 64, 78, 15);
		contentPane_1.add(lblNombre);
		
		textField_Nombre = new JTextField();
		textField_Nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_Apellidos.grabFocus();
			}
		});
		textField_Nombre.setColumns(10);
		textField_Nombre.setBounds(101, 62, 114, 19);
		contentPane_1.add(textField_Nombre);
		
		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setBounds(12, 99, 78, 21);
		contentPane_1.add(lblApellidos);
		
		textField_Apellidos = new JTextField();
		textField_Apellidos.setColumns(10);
		textField_Apellidos.setBounds(101, 99, 114, 19);
		contentPane_1.add(textField_Apellidos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 158, 384, 159);
		contentPane_1.add(scrollPane);
		
		table = new JTable() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(
				new javax.swing.table.DefaultTableModel(new Object[][] {}, new String[] { "ID", "Nombre", "Apellidos", "Email" }));
		scrollPane.setViewportView(table);
		
		/* Centrar texto de la tabla */
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		table.getColumnModel().getColumn(2).setCellRenderer(tcr);
		table.getColumnModel().getColumn(3).setCellRenderer(tcr);
		
		button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCajas();
			}
		});
		button_Cancelar.setMnemonic('c');
		button_Cancelar.setBounds(12, 353, 117, 25);
		contentPane_1.add(button_Cancelar);
		
		button_Borrar = new JButton("Borrar");
		button_Borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* CONTROLAR QUE CUANDO NO SELECCIONO NINGUNA FILA Y LE DOY A BORRAR */
//				String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),1));
//				if (dato.equals("") || dato == null) { // MAAAAL
//					JOptionPane.showMessageDialog(null, "Para borrar selecciona un elemento de la tabla.");
//				} else {
					int j = JOptionPane.showConfirmDialog(null, "¿Deseas borrar este cliente?", "Atención",
							JOptionPane.INFORMATION_MESSAGE);
					if (j == JOptionPane.YES_OPTION) {
						String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),0));
						try {
							PersistenciaMySQL.borrarCliente(dato);
							limpiarCajas();
						} catch (SQLException e1) {
							System.err.println("Error al borrar el género.");
							e1.printStackTrace();
						}
					}
					if (j == JOptionPane.NO_OPTION) {
						limpiarCajas();
					}
//				}
			}
		});
		button_Borrar.setMnemonic('b');
		button_Borrar.setEnabled(false);
		button_Borrar.setBounds(151, 353, 117, 25);
		contentPane_1.add(button_Borrar);
		
		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (textField_Email.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "No puedes guardar un cliente sin un email.");
				} else {

				}
				int j = JOptionPane.showConfirmDialog(null, "¿Deseas guardar este cliente?", "Atención",
						JOptionPane.INFORMATION_MESSAGE);
				if (j == JOptionPane.YES_OPTION) {
					try {
						PersistenciaMySQL.guardarCliente(textField_Nombre.getText(), textField_Apellidos.getText(), textField_Email.getText());
					} catch (Exception e1) {
						System.err.println("Error al guardar el cliente.");
						e1.printStackTrace();
					}
					limpiarCajas();
				}
				if (j == JOptionPane.NO_OPTION) {
					limpiarCajas();
				}
			}
		});
		button_Guardar.setMnemonic('g');
		button_Guardar.setEnabled(false);
		button_Guardar.setBounds(279, 353, 117, 25);
		contentPane_1.add(button_Guardar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setMnemonic('b');
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					ArrayList<String> alCliente = new ArrayList<>();
					if (!textField_Nombre.getText().equals("")) {
						try {
							alCliente = PersistenciaMySQL.buscarCliente(textField_Nombre.getText());
							tableModel = (DefaultTableModel) table.getModel();
							if (alCliente.size() != 0) {
								for (int i = 0; i < alCliente.size(); i++) {
									Object[] fila = new Object[4];
									fila[0] = alCliente.get(i).split(";")[0];
									fila[1] = alCliente.get(i).split(";")[1];
									fila[2] = alCliente.get(i).split(";")[2];
									fila[3] = alCliente.get(i).split(";")[3];
									tableModel.addRow(fila);
								}
								textField_Nombre.setEnabled(false);
								button_Borrar.setEnabled(true);
							} else {
								JOptionPane.showMessageDialog(null, "No existe ningún cliente que contengan esas letras.");
								button_Guardar.setEnabled(true);
							}
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							System.err.println("Error al buscar el cliente.");
							e2.printStackTrace();
						}
					} else {
						try {
							alCliente = PersistenciaMySQL.buscarCliente(textField_Nombre.getText());

							tableModel = (DefaultTableModel) table.getModel();
							if (alCliente.size() != 0) {
								for (int i = 0; i < alCliente.size(); i++) {
									Object[] fila = new Object[4];
									fila[0] = alCliente.get(i).split(";")[0];
									fila[1] = alCliente.get(i).split(";")[1];
									fila[2] = alCliente.get(i).split(";")[2];
									fila[3] = alCliente.get(i).split(";")[3];
									tableModel.addRow(fila);
								}
								button_Borrar.setEnabled(true);
							} else {
								JOptionPane.showMessageDialog(null, "No existe ningún cliente.");
								button_Guardar.setEnabled(true);
							}
						} catch (SQLException e3) {
							// TODO Auto-generated catch block
							e3.printStackTrace();
						}

					}
				}
		});
		btnBuscar.setBounds(289, 43, 89, 56);
		contentPane_1.add(btnBuscar);
	}
	
	private void limpiarCajas() {
		textField_Email.grabFocus();
		textField_Email.setText("");
		textField_Nombre.setEnabled(true);
		textField_Nombre.setText("");
		textField_Apellidos.setText("");		
		button_Borrar.setEnabled(false);
		button_Guardar.setEnabled(false);
		limpiarTabla(table);
	}

	public void limpiarTabla(JTable tabla) {
		try {
			DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
			int filas = tabla.getRowCount();
			for (int i = 0; filas > i; i++) {
				modelo.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
		}
	}
}