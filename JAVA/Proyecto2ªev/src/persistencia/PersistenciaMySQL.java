package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JOptionPane;

import com.mysql.jdbc.PreparedStatement;

public class PersistenciaMySQL implements Persistencia {

	static Connection con;

	public PersistenciaMySQL(String servidor, String baseDatos, String usuario, String password) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://" + servidor + "/" + baseDatos, usuario, password);
	}

	public static void guardarGenero(Genero g) throws Exception {
		Connection conexion = con;

		String insert = "INSERT INTO genero (descripcion) VALUES ('" + g.descripcion + "')";

		java.sql.PreparedStatement pst = conexion.prepareStatement(insert);
		pst.executeUpdate();
		pst.close();
	}
	
	
	public static ArrayList<String> buscarGenero(String descripcion) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");
		String buscar = null;
		if (!descripcion.equals("")) {
			buscar = "SELECT * FROM genero WHERE descripcion LIKE '%" + descripcion + "%'";
		} else {
			buscar = "SELECT * FROM genero";
		}
		
		java.sql.Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(buscar);
        ArrayList<String> alGenero = new ArrayList<String>();
        while (rs.next()) {
        	alGenero.add(rs.getInt(1) + "; " + rs.getString(2));
        }
        rs.close();
        st.close();
		
		return alGenero;
	}
	

	public static void borrarGenero(String seleccion) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");

		String eliminar = "DELETE FROM genero WHERE descripcion = '" + seleccion + "'";
		
		java.sql.PreparedStatement pst = con.prepareStatement(eliminar);
		pst.executeUpdate();

        pst.close();
	}

	@Override
	public ArrayList<Genero> listarGeneros(String filtro) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static ArrayList<String> buscarActor(String descripcion) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");
		String buscar = null;
		if (!descripcion.equals("")) {
			buscar = "SELECT * FROM actor WHERE nombre LIKE '%" + descripcion + "%'";
		} else {
			buscar = "SELECT * FROM actor";
		}
		
		java.sql.Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(buscar);
        ArrayList<String> alActor = new ArrayList<String>();
        while (rs.next()) {
        	alActor.add(rs.getInt(1) + ";" + rs.getString(2) + ";" + rs.getDate(3));
        }
        rs.close();
        st.close();
		
		return alActor;
	}

	public static void guardarActor(String nombre, String fecha) throws Exception { /* HAY QUE METER LA FECHA QUE ESCRIBIMOS */
		Connection conexion = con;
		
		String insert = "INSERT INTO actor (nombre, fechanac) VALUES ('" + nombre + "', '" + fecha + "')";

		java.sql.PreparedStatement pst = conexion.prepareStatement(insert);
		pst.executeUpdate();
		pst.close();
	}
	
	public static void borrarActor(String seleccion) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");

		String eliminar = "DELETE FROM actor WHERE nombre = '" + seleccion + "'";
		
		java.sql.PreparedStatement pst = con.prepareStatement(eliminar);
		pst.executeUpdate();

        pst.close();
	}
	
	public static ArrayList<String> buscarCliente(String nombre) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");
		String buscar = null;
		if (!nombre.equals("")) {
			buscar = "SELECT * FROM cliente WHERE nombre LIKE '%" + nombre + "%'";
		} else {
			buscar = "SELECT * FROM cliente";
		}
		
		java.sql.Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(buscar);
        ArrayList<String> alCliente = new ArrayList<String>();
        while (rs.next()) {
        	alCliente.add(rs.getInt(1) + ";" + rs.getString(2) + ";" + rs.getString(3) + ";" + rs.getString(4));
        }
        rs.close();
        st.close();
		
		return alCliente;
	}
	
	public static void guardarCliente(String nombre, String apellido, String email) throws Exception { /* HAY QUE METER LA FECHA QUE ESCRIBIMOS */
		Connection conexion = con;
		
		String insert = "INSERT INTO cliente (nombre, apellidos, email) VALUES ('" + nombre + "', '" + apellido + "', '" + email + "')";

		java.sql.PreparedStatement pst = conexion.prepareStatement(insert);
		pst.executeUpdate();
		pst.close();
	}
	
	public static void borrarCliente(String seleccion) throws SQLException {
		con = DriverManager.getConnection("jdbc:mysql://localhost/videoclub", "root", "");

		String eliminar = "DELETE FROM cliente WHERE idcliente = '" + seleccion + "'";
		
		java.sql.PreparedStatement pst = con.prepareStatement(eliminar);
		pst.executeUpdate();

        pst.close();
	}

}
