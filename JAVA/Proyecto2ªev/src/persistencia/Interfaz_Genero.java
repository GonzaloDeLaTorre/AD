package persistencia;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ScrollPane;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JTextField;
import javax.swing.Scrollable;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

public class Interfaz_Genero extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Id;
	private static JTextField textField_Descripcion;
	private JButton btnBorrar;
	private JTable table;
	private DefaultTableModel tableModel;
	private ArrayList<String> alGenero;

	Persistencia per = PrincipalGUI.per;
	// static javax.swing.JFrame.padre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Genero frame = new Interfaz_Genero();
					frame.setVisible(true);
					textField_Descripcion.grabFocus();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interfaz_Genero() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 547, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(12, 33, 38, 15);
		contentPane.add(lblId);

		textField_Id = new JTextField();
		textField_Id.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				textField_Id.setText(""+tableModel.getValueAt(0, 0)); // MAL !! Es para que se ponga la caja de texto ID con su id correspondiente
			}
		});
		textField_Id.setEditable(false);
		textField_Id.setBounds(101, 31, 114, 19);
		contentPane.add(textField_Id);
		textField_Id.setColumns(10);

		JLabel lblDescripcin = new JLabel("Descripción:");
		lblDescripcin.setBounds(12, 70, 114, 15);
		contentPane.add(lblDescripcin);

		textField_Descripcion = new JTextField();
		textField_Descripcion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!textField_Descripcion.getText().equals("")) {
					try {
						ArrayList<String> alGenero = new ArrayList<>();
						alGenero = PersistenciaMySQL.buscarGenero(textField_Descripcion.getText());
						tableModel = (DefaultTableModel) table.getModel();
						if (alGenero.size() != 0) {
							for (int i = 0; i < alGenero.size(); i++) {
								Object[] fila = new Object[2];
								fila[0] = alGenero.get(i).split("; ")[0];
								fila[1] = alGenero.get(i).split("; ")[1];
								tableModel.addRow(fila);
							}
							textField_Descripcion.setEnabled(false);
							btnBorrar.setEnabled(true);
						} else {
							int j = JOptionPane.showConfirmDialog(null,
									"No existe ningún género que contengan esas letras.\n¿Deseas guardarlo?",
									"Atención", JOptionPane.INFORMATION_MESSAGE);
							if (j == JOptionPane.YES_OPTION) {
								Genero g = new Genero(textField_Descripcion.getText());
								PersistenciaMySQL.guardarGenero(g);
								limpiarCajas();
							}
							if (j == JOptionPane.NO_OPTION) {
								limpiarCajas();
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.err.println("Error al buscar el género.");
						e.printStackTrace();
					}
				} else {
					alGenero = new ArrayList<>();
					try {
						alGenero = PersistenciaMySQL.buscarGenero(textField_Descripcion.getText());

						DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
						if (alGenero.size() != 0) {
							for (int i = 0; i < alGenero.size(); i++) {
								Object[] fila = new Object[2];
								fila[0] = alGenero.get(i).split("; ")[0];
								fila[1] = alGenero.get(i).split("; ")[1];
								tableModel.addRow(fila);
							}
							textField_Descripcion.setEnabled(false);
							btnBorrar.setEnabled(true);
						} else {
							JOptionPane.showMessageDialog(null, "No existe ningún género.");
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});
		textField_Descripcion.setBounds(101, 68, 114, 19);
		contentPane.add(textField_Descripcion);
		textField_Descripcion.setColumns(10);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setMnemonic('c');
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCajas();
			}
		});
		btnCancelar.setBounds(50, 215, 117, 25);
		contentPane.add(btnCancelar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.setMnemonic('b');
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* CONTROLAR QUE CUANDO NO SELECCIONO NINGUNA FILA Y LE DOY A BORRAR */
//				String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),1));
//				if (dato.equals("") || dato == null) { // MAAAAL
//					JOptionPane.showMessageDialog(null, "Para borrar selecciona un elemento de la tabla.");
//				} else {
					int j = JOptionPane.showConfirmDialog(null, "¿Deseas borrar este género?", "Atención",
							JOptionPane.INFORMATION_MESSAGE);
					if (j == JOptionPane.YES_OPTION) {
						String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),1));
						try {
							PersistenciaMySQL.borrarGenero(dato);
							limpiarCajas();
						} catch (SQLException e1) {
							System.err.println("Error al borrar el género.");
							e1.printStackTrace();
						}
					}
					if (j == JOptionPane.NO_OPTION) {
						limpiarCajas();
					}
//				}
			}
		});
		btnBorrar.setEnabled(false);
		btnBorrar.setBounds(225, 215, 117, 25);
		contentPane.add(btnBorrar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(265, 33, 245, 159);
		contentPane.add(scrollPane);

		table = new JTable() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(
				new javax.swing.table.DefaultTableModel(new Object[][] {}, new String[] { "ID", "Descripción" }));
		scrollPane.setViewportView(table);
		
		/* Centrar texto de la tabla */
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);

	}

	private void limpiarCajas() {
		textField_Descripcion.grabFocus();
		textField_Descripcion.setEnabled(true);
		textField_Descripcion.setText("");
		textField_Id.setText("");
		btnBorrar.setEnabled(false);
		limpiarTabla(table);
	}

	public void limpiarTabla(JTable tabla) {
		try {
			DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
			int filas = tabla.getRowCount();
			for (int i = 0; filas > i; i++) {
				modelo.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
		}
	}
}
