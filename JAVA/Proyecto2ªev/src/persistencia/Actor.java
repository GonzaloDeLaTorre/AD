package persistencia;

public class Actor {

	int idactor;
	String nombre;
	String fechanac;
	
	public Actor(String nombre) {
		super();
//		this.idactor = idactor;
		this.nombre = nombre;
//		this.fechanac = fechanac;
	}

	public int getIdactor() {
		return idactor;
	}

	public void setIdactor(int idactor) {
		this.idactor = idactor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFechanac() {
		return fechanac;
	}

	public void setFechanac(String fechanac) {
		this.fechanac = fechanac;
	}
	
}
