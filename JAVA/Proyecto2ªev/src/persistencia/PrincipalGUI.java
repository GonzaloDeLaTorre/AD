package persistencia;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import java.awt.event.ActionListener;
import java.io.InputStreamReader;
import java.util.Properties;
import java.awt.event.ActionEvent;

public class PrincipalGUI extends JFrame {

	static Persistencia per;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {					
					Properties prop=new Properties();
					prop.load(new InputStreamReader(PrincipalGUI.class.getResourceAsStream("CFG.INI"))); //CFG.INI se buscará en el mismo paquete que la clase PrincipalGUI
					String tipoPersistencia=prop.getProperty("tipoPersistencia");
					
//					Persistencia per = null;
					switch (tipoPersistencia){
					   case "mysqlJDBC":
					      String servidor=prop.getProperty("mysqlJDBC.servidor");
					      String baseDatos=prop.getProperty("mysqlJDBC.basedatos");
					      String usuario=prop.getProperty("mysqlJDBC.usuario");
					      String password=prop.getProperty("mysqlJDBC.password");
					      try {
					    	  per=new PersistenciaMySQL(servidor,baseDatos,usuario,password);
					      } catch (Exception e) {
							  JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion", JOptionPane.ERROR_MESSAGE);
							  System.exit(0);
						}					      
					      break;
					   case "hibernate": 
					      per=new PersistenciaHibernate(prop.getProperty("hibernate.archivoCFG"));
					      break;
					   default: //ERROR
						   System.err.println("Error, no conecta a ningun tipo de bbdd.");						   
					}	
					
					PrincipalGUI frame = new PrincipalGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalGUI() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu_1 = new JMenu("Gestión");
		mnNewMenu_1.setMnemonic('g');
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmAlquiler = new JMenuItem("Alquiler");
		mntmAlquiler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz_Alquiler alquiler = new Interfaz_Alquiler();
				alquiler.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmAlquiler);
		
		JMenuItem mntmDevolucin = new JMenuItem("Devolución");
		mntmDevolucin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz_Devolucion devolucion = new Interfaz_Devolucion();
				devolucion.setVisible(true);
			}
		});
		mnNewMenu_1.add(mntmDevolucin);
		
		JMenu mnNewMenu = new JMenu("Mantenimiento");
		mnNewMenu.setMnemonic('b');
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmActores = new JMenuItem("Actores");
		mntmActores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Interfaz_Actores actores = new Interfaz_Actores();
				actores.setVisible(true);
			}
		});
		mnNewMenu.add(mntmActores);
		
		JMenuItem mntmClientes = new JMenuItem("Clientes");
		mntmClientes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Interfaz_Clientes clientes = new Interfaz_Clientes();
				clientes.setVisible(true);
			}
		});
		mnNewMenu.add(mntmClientes);
		
		JMenuItem mntmEjemplares = new JMenuItem("Ejemplares");
		mntmEjemplares.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz_Ejemplares ejemplares = new Interfaz_Ejemplares();
				ejemplares.setVisible(true);
			}
		});
		mnNewMenu.add(mntmEjemplares);
		
		JMenuItem mntmGnero = new JMenuItem("Géneros");
		mntmGnero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Interfaz_Genero genero = new Interfaz_Genero();
				genero.setVisible(true);
			}
		});
		mnNewMenu.add(mntmGnero);
		
		JMenuItem mntmPelicula = new JMenuItem("Películas");
		mntmPelicula.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz_Peliculas peliculas = new Interfaz_Peliculas();
				peliculas.setVisible(true);
			}
		});
		mnNewMenu.add(mntmPelicula);
		
		JMenuItem mntmTarifas = new JMenuItem("Tarifas");
		mntmTarifas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Interfaz_Tarifas tarifas = new Interfaz_Tarifas();
				tarifas.setVisible(true);
			}
		});
		mnNewMenu.add(mntmTarifas);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
}
