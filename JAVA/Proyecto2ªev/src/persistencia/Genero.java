package persistencia;

public class Genero {

	int idgenero;
	String descripcion;
	
	public Genero(String descripcion) {
		super();
//		this.idgenero = idgenero;
		this.descripcion = descripcion;
	}
	
	public int getIdgenero() {
		return idgenero;
	}
	public void setIdgenero(int idgenero) {
		this.idgenero = idgenero;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
