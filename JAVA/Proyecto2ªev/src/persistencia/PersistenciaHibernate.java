package persistencia;

import java.sql.SQLException;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class PersistenciaHibernate implements Persistencia {

	public PersistenciaHibernate(String property) {
		// TODO Auto-generated constructor stub
	}

	public static void guardarGenero(int idgenero, String descripcion) throws Exception {
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		// Abrimos sesion para comprobar si existe en la bbdd el profesor
		Session session = sessionFactory.openSession();

		Genero g = new Genero(descripcion);

		session.beginTransaction();

		session.save(g);

		session.getTransaction().commit();
		session.close();

		/*
		 * static { try { //Configuration configuration = new
		 * Configuration().configure(); standardServiceRegistry = new
		 * StandardServiceRegistryBuilder().configure("hibernate.cfg.xml") .build();
		 * Metadata metadata = new MetadataSources(standardServiceRegistry)
		 * .getMetadataBuilder().build(); sessionFactory =
		 * metadata.getSessionFactoryBuilder().build();
		 * 
		 * }catch(HibernateException exception) {
		 * System.out.println("problem creating session factory!"); } }
		 * 
		 * public static SessionFactory getSessionFactory(){
		 * 
		 * return sessionFactory; }
		 */
	}

	public void borrarGenero(Genero g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Genero> listarGeneros(String filtro) {
		// TODO Auto-generated method stub
		return null;
	}

}
