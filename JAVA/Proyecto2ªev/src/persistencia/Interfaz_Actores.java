package persistencia;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Interfaz_Actores extends JFrame {

	private JPanel contentPane;
	private JTextField textField_Id;
	private static JTextField textField_Nombre;
	private JTextField textField_FechaNac;
	private JTable table;
	private DefaultTableModel tableModel;
//	private ArrayList<String> alGenero;
	
	Persistencia per = PrincipalGUI.per;
	private JButton button_Guardar;
	private JButton button_Borrar;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz_Actores frame = new Interfaz_Actores();
					frame.setVisible(true);
					textField_Nombre.grabFocus();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Interfaz_Actores() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 429);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField_Id = new JTextField();
		textField_Id.setEditable(false);
		textField_Id.setColumns(10);
		textField_Id.setBounds(101, 25, 114, 19);
		contentPane.add(textField_Id);
		
		JLabel label = new JLabel("ID:");
		label.setBounds(12, 27, 38, 15);
		contentPane.add(label);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(12, 64, 114, 15);
		contentPane.add(lblNombre);
		
		textField_Nombre = new JTextField();
		textField_Nombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<String> alActor = new ArrayList<>();
				if (!textField_Nombre.getText().equals("")) {
					try {
//						ArrayList<String> alActor = new ArrayList<>();
						alActor = PersistenciaMySQL.buscarActor(textField_Nombre.getText());
						tableModel = (DefaultTableModel) table.getModel();
						if (alActor.size() != 0) {
							for (int i = 0; i < alActor.size(); i++) {
								Object[] fila = new Object[3];
								fila[0] = alActor.get(i).split(";")[0];
								fila[1] = alActor.get(i).split(";")[1];
								fila[2] = alActor.get(i).split(";")[2];
								tableModel.addRow(fila);
							}
							textField_Nombre.setEnabled(false);
							button_Borrar.setEnabled(true);
						} else {
							JOptionPane.showMessageDialog(null, "No existe ningún nombre de actor que contengan esas letras.");
							button_Guardar.setEnabled(true);
							textField_FechaNac.setEditable(true);
							textField_FechaNac.grabFocus();
						}
					} catch (Exception e2) {
						// TODO Auto-generated catch block
						System.err.println("Error al buscar el actor.");
						e2.printStackTrace();
					}
				} else {
					try {
						alActor = PersistenciaMySQL.buscarActor(textField_Nombre.getText());

						DefaultTableModel tableModel = (DefaultTableModel) table.getModel();
						if (alActor.size() != 0) {
							for (int i = 0; i < alActor.size(); i++) {
								Object[] fila = new Object[3];
								fila[0] = alActor.get(i).split(";")[0];
								fila[1] = alActor.get(i).split(";")[1];
								fila[2] = alActor.get(i).split(";")[2];
								tableModel.addRow(fila);
							}
							textField_Nombre.setEnabled(false);
							textField_FechaNac.setEnabled(false);
							button_Borrar.setEnabled(true);
						} else {
							JOptionPane.showMessageDialog(null, "No existe ningún actor.");
						}
					} catch (SQLException e3) {
						// TODO Auto-generated catch block
						e3.printStackTrace();
					}

				}
			}
		});
		textField_Nombre.setColumns(10);
		textField_Nombre.setBounds(101, 62, 114, 19);
		contentPane.add(textField_Nombre);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha nacimiento:");
		lblFechaNacimiento.setBounds(12, 99, 136, 21);
		contentPane.add(lblFechaNacimiento);
		
		textField_FechaNac = new JTextField();
		textField_FechaNac.setEditable(false);
		textField_FechaNac.setColumns(10);
		textField_FechaNac.setBounds(151, 101, 114, 19);
		contentPane.add(textField_FechaNac);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 158, 384, 159);
		contentPane.add(scrollPane);

		table = new JTable() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(
				new javax.swing.table.DefaultTableModel(new Object[][] {}, new String[] { "ID", "Nombre", "Fecha Nacimiento" }));
		scrollPane.setViewportView(table);
		
		/* Centrar texto de la tabla */
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(tcr);
		table.getColumnModel().getColumn(1).setCellRenderer(tcr);
		table.getColumnModel().getColumn(2).setCellRenderer(tcr);

		
		JButton button_Cancelar = new JButton("Cancelar");
		button_Cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCajas();
			}
		});
		button_Cancelar.setMnemonic('c');
		button_Cancelar.setBounds(12, 353, 117, 25);
		contentPane.add(button_Cancelar);
		
		button_Borrar = new JButton("Borrar");
		button_Borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* CONTROLAR QUE CUANDO NO SELECCIONO NINGUNA FILA Y LE DOY A BORRAR */
//				String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),1));
//				if (dato.equals("") || dato == null) { // MAAAAL
//					JOptionPane.showMessageDialog(null, "Para borrar selecciona un elemento de la tabla.");
//				} else {
					int j = JOptionPane.showConfirmDialog(null, "¿Deseas borrar este actor?", "Atención",
							JOptionPane.INFORMATION_MESSAGE);
					if (j == JOptionPane.YES_OPTION) {
						String dato = String.valueOf(tableModel.getValueAt(table.getSelectedRow(),1));
						try {
							PersistenciaMySQL.borrarActor(dato);
							limpiarCajas();
						} catch (SQLException e1) {
							System.err.println("Error al borrar el género.");
							e1.printStackTrace();
						}
					}
					if (j == JOptionPane.NO_OPTION) {
						limpiarCajas();
					}
//				}
			}
		});
		button_Borrar.setMnemonic('b');
		button_Borrar.setEnabled(false);
		button_Borrar.setBounds(151, 353, 117, 25);
		contentPane.add(button_Borrar);
		
		button_Guardar = new JButton("Guardar");
		button_Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int j = JOptionPane.showConfirmDialog(null, "¿Deseas guardar este actor?", "Atención",
						JOptionPane.INFORMATION_MESSAGE);
				if (j == JOptionPane.YES_OPTION) {
					try {
						PersistenciaMySQL.guardarActor(textField_Nombre.getText(), textField_FechaNac.getText());
					} catch (Exception e1) {
						System.err.println("Error al guardar el actor.");
						e1.printStackTrace();
					}
					limpiarCajas();
				}
				if (j == JOptionPane.NO_OPTION) {
					limpiarCajas();
				}
			}
		});
		button_Guardar.setEnabled(false);
		button_Guardar.setMnemonic('g');
		button_Guardar.setBounds(279, 353, 117, 25);
		contentPane.add(button_Guardar);
	}
	
	private void limpiarCajas() {
		textField_Nombre.grabFocus();
		textField_Nombre.setEnabled(true);
		textField_Nombre.setText("");
		textField_Id.setText("");
		textField_FechaNac.setText("");
		button_Borrar.setEnabled(false);
		button_Guardar.setEnabled(false);
		limpiarTabla(table);
	}

	public void limpiarTabla(JTable tabla) {
		try {
			DefaultTableModel modelo = (DefaultTableModel) tabla.getModel();
			int filas = tabla.getRowCount();
			for (int i = 0; filas > i; i++) {
				modelo.removeRow(0);
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
		}
	}
}
