package ej3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ModificacionXML {
	
	private static String buscarDivisa(String divisa) throws IOException {
		BufferedReader bfr=new BufferedReader(new FileReader(new File("Divisas.csv")));
		String linea=null;
		String valor=null;
		//Leemos lineas de texto
		while ((linea=bfr.readLine())!=null) {
			//Obtiene los campos de linea (separados por ;) y los muestra
			StringTokenizer st=new StringTokenizer(linea,";");
			String nombre=st.nextToken();
			String v=st.nextToken();
			if (divisa.equals(nombre)){
				valor=v;
				break; //Encontrada, dejamos de buscar
			}
		}
		//Cerramos fichero
		bfr.close();
		return valor;
	}

	public static double redondear( double numero, int decimales )
	{
		return Math.rint(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
	}
	
	public static void main(String[] args) {

		try{
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse("desayunos.xml");
				
		//Obtenemos todos los elementos <descripcion> mediante XPATH	
		NodeList lista=(NodeList) (XPathFactory.newInstance().newXPath().evaluate("//descripcion", doc,XPathConstants.NODESET));
		for (int i = 0; i < lista.getLength(); i++) {
			lista.item(i).getParentNode().removeChild(lista.item(i)); //Borra el elemento(descripcion) desde el padre(desayuno)
		}
		
		//Obtenemos total calorias mediante XPATH			
		lista=(NodeList) (XPathFactory.newInstance().newXPath().evaluate("/menu_desayunos/desayuno/calorias", doc,XPathConstants.NODESET));
		int total=0;
		for (int i = 0; i < lista.getLength(); i++) {
			if (lista.item(i).getNodeType()==Node.ELEMENT_NODE)
				total += Integer.parseInt(lista.item(i).getTextContent());
		}
		
        //Crea elemento total_calorias y lo a�ade a raiz(al final)
		Element raiz=doc.getDocumentElement();
        Element hijo = doc.createElement("total_calorias");
        hijo.setTextContent(String.valueOf(total));
        raiz.appendChild(hijo); 
        
		//Obtenemos los precios 
        lista=(NodeList) (XPathFactory.newInstance().newXPath().evaluate("/menu_desayunos/desayuno/precio", doc,XPathConstants.NODESET));
		for (int i = 0; i < lista.getLength(); i++) {
			if (lista.item(i).getNodeType()==Node.ELEMENT_NODE){
				Element precio=(Element)lista.item(i);
				//Obtenemos la divisa del precio
				String divisa=precio.getAttribute("moneda");
				String cambio=buscarDivisa(divisa);
				if (cambio!=null){
					//Cambia la moneda y recalcula el precio
					double cambioD=Double.valueOf(cambio);
					double precioD=Double.valueOf(precio.getTextContent());
					precio.setAttribute("moneda", "euro");
					precio.setTextContent(String.valueOf(redondear(precioD/cambioD,2)));
				}
			}
		}
		
	
		// Escribe arbol DOM a fichero XML
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		transformerFactory = TransformerFactory.newInstance();
		transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("desayunos.modificado.xml"));
		transformer.transform(source, result);

		System.out.println("Fin programa.");

	} catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	} catch (TransformerException tfe) {
		tfe.printStackTrace();
	} catch (SAXException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (XPathExpressionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}

