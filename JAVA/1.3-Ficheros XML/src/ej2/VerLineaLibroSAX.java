package ej2;
//Contruye indice.xml a partir de los ficheros de texto que haya en C:\biblioteca

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.Scanner;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class VerLineaLibroSAX {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String pathBibl = "/home/alumno/biblioteca";

	    try {
			Scanner s = new Scanner(System.in);
			System.out.print("Introduzca titulo del libro:");
			String titulo = s.nextLine();
	    	
	        File inputFile = new File(pathBibl+"/indice.xml");
	        SAXParserFactory factory = SAXParserFactory.newInstance();
	        SAXParser saxParser = factory.newSAXParser();
	        ManejadorSAX manejador = new ManejadorSAX(titulo,null);
	        //Primer parse para buscar el libro
	        saxParser.parse(inputFile, manejador);
	        if (!manejador.isLibroEncontrado())
			{
				System.out.println("Libro no encontrado");
			}
			else{
				System.out.print("Introduzca numero de linea:");
				String num = s.next();
				manejador = new ManejadorSAX(titulo,num);
				//Segundo parse para buscar el libro
		        saxParser.parse(inputFile, manejador);
				if (!manejador.isLineaEncontrada())
				{
					System.out.println("Linea no encontrada");
				}
				else{
					RandomAccessFile raf = new RandomAccessFile(pathBibl+"/"+titulo, "r");
					raf.seek(Integer.parseInt(manejador.getPosicion())); //Accedemos de forma directa a la posicion obtenida
					String texto = raf.readLine();
					System.out.println(manejador.getPosicion()+" --> "+texto);
					raf.close();
				}
			}
	      } catch (Exception e) {
	         e.printStackTrace();
	      }

		System.out.println("Programa finalizado.");
	}
}



