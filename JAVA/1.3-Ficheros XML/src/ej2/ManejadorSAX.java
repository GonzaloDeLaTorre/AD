package ej2;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class ManejadorSAX extends DefaultHandler {
	String titulo; //Título a buscar
	String linea; //Linea a buscar dentro del titulo. Si es null sólo buscamos el titulo
	String posicion; //Posicion buscada
	boolean libroEncontrado;
	boolean lineaEncontrada;
	boolean dentroLibro; //A true cuando <linea> pertenece al libro buscado
	boolean dentroLinea; //A true cuando el texto(characters) pertenece a la linea buscada 
	
	public ManejadorSAX(String titulo, String num) {
		this.titulo=titulo;
		this.linea=num;
		libroEncontrado=false;
		lineaEncontrada=false;
		dentroLibro=false;
		dentroLinea=false;
		posicion=null;
	}

	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		if (name.equals("libro")){ //Etiqueta <libro>
			if (attributes.getValue("titulo").equals(titulo)){
				libroEncontrado=true;
				dentroLibro=true;
			}
		}
		if (linea!=null && name.equals("linea")&&dentroLibro){ //Etiqueta <linea> dentro del titulo buscado<libro>
			if (attributes.getValue("num").equals(linea)){
				lineaEncontrada=true;
				dentroLinea=true;
			}
		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if (dentroLinea){
			posicion=String.valueOf(ch, start, length);
		}
	}

	public void endElement(String uri, String localName, String name)
			throws SAXException {
		if (name.equals("libro")) //Etiqueta </libro>
			dentroLibro=false;
		if (name.equals("linea")) //Etiqueta </linea>
			dentroLinea=false;
	}

	public boolean isLibroEncontrado() {
		return libroEncontrado;
	}

	public boolean isLineaEncontrada() {
		return lineaEncontrada;
	}

	public String getPosicion() {
		return posicion;
	}

}