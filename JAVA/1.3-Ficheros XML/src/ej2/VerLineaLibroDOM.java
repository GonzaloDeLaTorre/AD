package ej2;
//Contruye indice.xml a partir de los ficheros de texto que haya en C:\biblioteca

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.Scanner;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class VerLineaLibroDOM {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
		String pathBibl = "/home/alumno/biblioteca";

		// Creamos arbol DOM
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.parse(pathBibl+"/indice.xml");

	
		Scanner s = new Scanner(System.in);
		System.out.print("Introduzca titulo del libro:");
		String titulo = s.nextLine();
		
		//Busca libro con XPATH
		Element el = (Element) (XPathFactory.newInstance().newXPath().evaluate("/biblioteca/libro[@titulo=\""+titulo+"\"]", doc,XPathConstants.NODE));
		if (el==null)
		{
			System.out.println("Libro no encontrado");
		}
		else{
			System.out.print("Introduzca numero de linea:");
			String num = s.next();
			el = (Element) (XPathFactory.newInstance().newXPath().evaluate("/biblioteca/libro[@titulo=\""+titulo+"\"]/linea[@num=\""+num+"\"]", doc,XPathConstants.NODE));
			if (el==null)
			{
				System.out.println("Linea no encontrada");
			}
			else{
				String pos=el.getTextContent();
				RandomAccessFile raf = new RandomAccessFile(pathBibl+"/"+titulo, "r");
				raf.seek(Integer.parseInt(pos));
				String texto = raf.readLine();
				System.out.println(pos+" --> "+texto);
				raf.close();
				
			}
		}
		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Programa finalizado.");
	};

}
