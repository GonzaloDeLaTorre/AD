package ej1;
//Contruye indice.xml a partir de los ficheros de texto que haya en /home/alumno/biblioteca

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.RandomAccessFile;
import java.util.Date;
import java.util.Scanner;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class IndexaTextos {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String path_bibl = "/home/alumno/biblioteca";
			
			File d = new File(path_bibl);
			if (!d.exists() || !d.isDirectory()) {
				System.out.println("No es un directorio.");
				System.exit(-1);
			}
			
			//Borra indice.xml
			File rutaIndice=new File(path_bibl+"/indice.xml");
			rutaIndice.delete();
			
			// Creamos arbol DOM
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Crea elemento raiz
			Element raiz = doc.createElement("biblioteca");
			doc.appendChild(raiz);
			
			// Busca libros
			File [] lista = d.listFiles();
			for (int i = 0; i < lista.length; i++) {
				// crea elemento libro, con atributo "titulo"
				Element libro = doc.createElement("libro");
				libro.setAttribute("titulo", lista[i].getName());
				raiz.appendChild(libro);
	
				RandomAccessFile raf = new RandomAccessFile(lista[i], "r");
				int cnt = 0;
				long pos = raf.getFilePointer();
				while (raf.readLine() != null) {
					cnt++;
					// crea elemento linea, con atributo "num"
					Element lin = doc.createElement("linea");
					lin.setAttribute("num", String.valueOf(cnt));
					lin.setTextContent(String.valueOf(pos));
					libro.appendChild(lin);
					
					pos = raf.getFilePointer();
				}
				raf.close();
	
				System.out.println("Indexado --> " + lista[i].getName());
	
			}
	
			// Escribe arbol DOM a fichero XML
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(rutaIndice);
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Programa finalizado.");
	};

}
