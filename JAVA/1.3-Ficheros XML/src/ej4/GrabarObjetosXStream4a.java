package ej4;

import java.io.*;
import java.util.ArrayList;



import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class GrabarObjetosXStream4a {

	public static void main(String[] args) throws IOException {
		Persona p1=new Persona(20,"Julio Perez",'M');
		p1.aniadirTrabajo(new Trabajo(1,"Albanil"));
		p1.aniadirTrabajo(new Trabajo(1,"Encofrador"));
		Persona p2=new Persona(21,"Ana Gutierrez",'F');
		p2.aniadirTrabajo(new Trabajo(2,"Ingeniero"));
		Persona p3=new Persona(84,"Emilia Rivilla",'F');
		p3.aniadirTrabajo(new Trabajo(2,"Profesora"));
		p3.aniadirTrabajo(new Trabajo(3,"Locutora de radio"));
		
		//Personas tiene un atributo que es una lista de personas.
		Personas pers=new Personas();
		pers.getLista().add(p1);
		pers.getLista().add(p2);
		pers.getLista().add(p3);
		
		//Grabamos en fichero de texto
		PrintWriter pw=new PrintWriter(new FileWriter(new File("Personas.xml")));
		//Escribimos cabecera XML, XStream no lo hace
		pw.println("<?xml version=\"1.0\"?>");
		
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("personas", Personas.class);
		//Evita que se escriba <lista> al generar XML a partir de un objeto Personas 
		xstream.addImplicitCollection(Personas.class, "lista", Persona.class);
		xstream.alias("persona", Persona.class);
		xstream.alias("trabajo", Trabajo.class);
		//Evita que se escriba <trabs> al generar XML a partir de un objeto Persona 
		xstream.addImplicitCollection(Persona.class, "trabs", Trabajo.class);
		
		//Se convierte la lista de Personas en XML y se escribe en fichero 
		xstream.toXML(pers,pw);
		//Cerramos fichero
		pw.close();
		
		System.out.println("=== Objetos guardados en Personas.xml con XStream ===");
	}
}
