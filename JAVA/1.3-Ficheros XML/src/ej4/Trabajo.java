package ej4;

import java.io.Serializable;


public class Trabajo{
	int tipo;
	String descripcion;
	public Trabajo(int tipo, String descripcion) {
		super();
		this.tipo = tipo;
		this.descripcion = descripcion;
	}

	public String toString() {
		return "Trabajo [tipo=" + tipo + ", descripcion=" + descripcion + "]";
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
