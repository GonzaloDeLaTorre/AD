package ej4;

import java.io.*;
import java.util.Iterator;
import java.util.StringTokenizer;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;



public class RecuperarObjetosXStream4b {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		//Leemos de fichero de texto
		BufferedReader bfr=new BufferedReader(new FileReader(new File(".xml")));
		//Saltamos cabecera XML, XStream no lo hace
		bfr.readLine();
		
		XStream xstream = new XStream(new DomDriver());
		xstream.alias("personas", Personas.class);
		//Evita que se escriba <lista> al generar XML a partir de un objeto Personas 
		xstream.addImplicitCollection(Personas.class, "lista", Persona.class);
		xstream.alias("persona", Persona.class);
		xstream.alias("trabajo", Trabajo.class);
		//Evita que se escriba <trabs> al generar XML a partir de un objeto Persona 
		xstream.addImplicitCollection(Persona.class, "trabs", Trabajo.class);
		
		//Se convierte la lista de Personas en XML y se escribe en fichero 
		Personas pers=(Personas) xstream.fromXML(bfr);
		//Cerramos fichero
		bfr.close();
		
		
		
		System.out.println("=== Objetos en Personas.xml (leidos con XStream) ===");
		for (Persona p : pers.getLista()) {
			System.out.println(p);
		}

	}
}
