package pdf1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import java.awt.Window.Type;
import javax.swing.ButtonGroup;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class Ej1 extends JFrame {

	private JPanel contentPane;
	private JButton btn_SeleccionarCarpeta;
	private JLabel lbl_TamañoEnBytes;
	private JRadioButton rdbtn_Mayores;
	private JRadioButton rdbtn_Menores;
	private JButton btn_Buscar;
	private JCheckBox chckbx_Ocultos;
	private JCheckBox chckbx_Subcarpetas;
	private JLabel lbl_ResultadosDeLa;
	private JTextField textField_Bytes;
	private JTextField textField_RutaSeleccionada;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextArea textArea_ResultadoDeLaBusqueda;
	File ruta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej1 frame = new Ej1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ej1() {
		setResizable(false);
		setTitle("Buscar por tamaño");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 585, 406);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		btn_SeleccionarCarpeta = new JButton("Seleccionar Carpeta");
		btn_SeleccionarCarpeta.setBounds(22, 25, 216, 25);
		btn_SeleccionarCarpeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarCarpeta();
			}

			private void seleccionarCarpeta() {
				JFileChooser fc=new JFileChooser("/home/alumno");
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int opcion=fc.showOpenDialog(null);
				if (opcion==JFileChooser.APPROVE_OPTION) {
					ruta=fc.getSelectedFile();
					textField_RutaSeleccionada.setText(ruta.getAbsolutePath());
					textField_Bytes.requestFocus();
					btn_Buscar.setEnabled(true);
				} else {
					JOptionPane.showMessageDialog(btn_SeleccionarCarpeta, "Selecciona el archivo", "Error", JOptionPane.ERROR_MESSAGE);
				}	
			}
		});
		
		contentPane.setLayout(null);
		contentPane.add(btn_SeleccionarCarpeta);
		
		lbl_TamañoEnBytes = new JLabel("Tamaño en bytes");
		lbl_TamañoEnBytes.setBounds(22, 76, 122, 25);
		contentPane.add(lbl_TamañoEnBytes);
		
		rdbtn_Mayores = new JRadioButton("Mayores");
		rdbtn_Mayores.setSelected(true);
		buttonGroup.add(rdbtn_Mayores);
		rdbtn_Mayores.setBounds(228, 77, 106, 23);
		contentPane.add(rdbtn_Mayores);
		
		rdbtn_Menores = new JRadioButton("Menores");
		buttonGroup.add(rdbtn_Menores);
		rdbtn_Menores.setBounds(228, 104, 93, 23);
		contentPane.add(rdbtn_Menores);
		
		btn_Buscar = new JButton("Buscar");
		btn_Buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscar(ruta);
			}

			private void buscar(File ruta) {
				File [] l=ruta.listFiles();
				String f="";
				if (l!=null) {
					for (int i = 0; i < l.length; i++) {
						f+=" "+l[i].getName()+"\n";
						if (chckbx_Subcarpetas.isSelected() && l[i].isDirectory()) {
							buscar(l[i]);
						}
					}
					textArea_ResultadoDeLaBusqueda.setText(f);
				}
			}
		});
		btn_Buscar.setEnabled(false);
		btn_Buscar.setBounds(467, 81, 93, 25);
		contentPane.add(btn_Buscar);
		
		chckbx_Ocultos = new JCheckBox("Ocultos");
		chckbx_Ocultos.setBounds(338, 71, 93, 35);
		contentPane.add(chckbx_Ocultos);
		
		chckbx_Subcarpetas = new JCheckBox("Subcarpetas");
		chckbx_Subcarpetas.setBounds(338, 104, 122, 23);
		contentPane.add(chckbx_Subcarpetas);
		
		lbl_ResultadosDeLa = new JLabel("Resultados de la búsqueda:");
		lbl_ResultadosDeLa.setBounds(29, 159, 228, 19);
		contentPane.add(lbl_ResultadosDeLa);
		
		textField_Bytes = new JTextField();
		textField_Bytes.setText("0");
		textField_Bytes.setBackground(Color.WHITE);
		textField_Bytes.setBounds(162, 76, 58, 25);
		contentPane.add(textField_Bytes);
		textField_Bytes.setColumns(10);
		
		textField_RutaSeleccionada = new JTextField();
		textField_RutaSeleccionada.setEditable(false);
		textField_RutaSeleccionada.setBounds(268, 25, 292, 28);
		contentPane.add(textField_RutaSeleccionada);
		textField_RutaSeleccionada.setColumns(10);
		
		textArea_ResultadoDeLaBusqueda = new JTextArea();
		textArea_ResultadoDeLaBusqueda.setEditable(false);
		textArea_ResultadoDeLaBusqueda.setBounds(22, 190, 538, 178);
		contentPane.add(textArea_ResultadoDeLaBusqueda);
	}
}
