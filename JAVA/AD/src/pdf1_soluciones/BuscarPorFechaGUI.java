package pdf1_soluciones;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import comun.Utilidades;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JTextArea;
import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JScrollPane;
import java.awt.Font;




public class BuscarPorFechaGUI extends JFrame { //ej2

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField ruta;
	private JTextField fecha;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnAntesde;
	private JTextArea textAreaRes;
	private JCheckBox chckbxIncluirOcultos;
	private JScrollPane scrollPane;
	private JCheckBox chckbxIncluirSubcarpetas;
	private JButton btnBuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarPorFechaGUI frame = new BuscarPorFechaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarPorFechaGUI() {
		setTitle("Buscar por fecha de modificaci\u00F3n");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 597, 507);
		setLocationRelativeTo(null); //Centra ventana en la pantalla
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Seleccionar Carpeta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonSeleccionar();
			}
		});
		btnNewButton.setBounds(10, 24, 194, 23);
		contentPane.add(btnNewButton);
		
		ruta = new JTextField();
		ruta.setEditable(false);
		ruta.setBounds(214, 25, 346, 20);
		contentPane.add(ruta);
		ruta.setColumns(10);
		
		JLabel lblTamaoEnBytes = new JLabel("DD-MM-AAAA");
		lblTamaoEnBytes.setBounds(10, 76, 98, 14);
		contentPane.add(lblTamaoEnBytes);
		
		fecha = new JTextField();
		fecha.setBounds(95, 73, 109, 20);
		contentPane.add(fecha);
		fecha.setColumns(10);
		
		rdbtnAntesde = new JRadioButton("Antes de");
		buttonGroup.add(rdbtnAntesde);
		rdbtnAntesde.setBounds(210, 72, 86, 23);
		contentPane.add(rdbtnAntesde);
		
		JRadioButton rdbtnDespuesDe = new JRadioButton("Despu\u00E9s de");
		rdbtnDespuesDe.setSelected(true);
		buttonGroup.add(rdbtnDespuesDe);
		rdbtnDespuesDe.setBounds(210, 98, 109, 23);
		contentPane.add(rdbtnDespuesDe);
		
		chckbxIncluirOcultos = new JCheckBox("Incluir Ocultos");
		chckbxIncluirOcultos.setBounds(321, 72, 109, 23);
		contentPane.add(chckbxIncluirOcultos);
		
		btnBuscar = new JButton("BUSCAR");
		btnBuscar.setEnabled(false);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				botonBuscar();
			}
		});
		btnBuscar.setBounds(471, 72, 89, 23);
		contentPane.add(btnBuscar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 170, 492, 243);
		contentPane.add(scrollPane);
		
		textAreaRes = new JTextArea();
		scrollPane.setViewportView(textAreaRes);
		
		JLabel lblResultadosDeLa = new JLabel("Resultados de la b\u00FAsqueda");
		lblResultadosDeLa.setBounds(25, 145, 181, 14);
		contentPane.add(lblResultadosDeLa);
		
		chckbxIncluirSubcarpetas = new JCheckBox("Incluir Subcarpetas");
		chckbxIncluirSubcarpetas.setFont(new Font("Tahoma", Font.PLAIN, 11));
		chckbxIncluirSubcarpetas.setBounds(321, 98, 176, 23);
		contentPane.add(chckbxIncluirSubcarpetas);
	}

	protected void botonBuscar() {
		textAreaRes.setText("");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		sdf.setLenient(false);
		Date d=null;
        try {
            d = sdf.parse(fecha.getText());
    		ArrayList<File> res=Utilidades.buscarArchivosPorFechaModif(new File(ruta.getText()), d.getTime(), rdbtnAntesde.isSelected(), chckbxIncluirOcultos.isSelected(), chckbxIncluirSubcarpetas.isSelected());	
    		String t="";
			//Muestra resultado de la búsqueda
			for (int i = 0; i < res.size(); i++)
				t+=Utilidades.mostrarInfoFileFechaHora(res.get(i))+"\n";
			textAreaRes.setText(t);
			JOptionPane.showMessageDialog(this, res.size()+" archivos encontrados.");
        } catch (ParseException e) {
        	Utilidades.notificaError(this, "Error al buscar", e, "Fecha incorrecta (DD-MM-YYY)");
        }
	}

	protected void botonSeleccionar() {
		ruta.setText("");
		textAreaRes.setText("");
		btnBuscar.setEnabled(false);
		JFileChooser fc=new JFileChooser(".");
		fc.setDialogTitle("Seleccione carpeta donde buscar");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int seleccion = fc.showOpenDialog(this);
		if (seleccion == JFileChooser.APPROVE_OPTION)
		{
			File f=fc.getSelectedFile();
			if (f.isDirectory()) {
				ruta.setText(fc.getSelectedFile().getAbsolutePath());
				btnBuscar.setEnabled(true);
			}
			else {
				Utilidades.notificaError(this, "Error al seleccionar carpeta", null, f.getAbsolutePath()+" no es una carpeta");			
			}
		} 
	}
}
