package pdf1_soluciones;

import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

public class BorrarRenombrarCrearGUI extends JFrame { //ej4

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private Component ventanaGUI=this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BorrarRenombrarCrearGUI frame = new BorrarRenombrarCrearGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BorrarRenombrarCrearGUI() {
		setTitle("Borrar, Renombrar, Crear");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 249);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCrear = new JButton("CREAR");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc=new JFileChooser("/");
				fc.setDialogTitle("Seleccione nombre de carpeta a crear");
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fc.setApproveButtonText("CREAR");
				int seleccion2 = fc.showSaveDialog(ventanaGUI);
				if (seleccion2 == JFileChooser.APPROVE_OPTION)
				{
					if (fc.getSelectedFile().mkdirs())
						JOptionPane.showMessageDialog(ventanaGUI,"Se ha creado: "+fc.getSelectedFile().getAbsolutePath());
					else
						JOptionPane.showMessageDialog(ventanaGUI, fc.getSelectedFile().getAbsolutePath(), "Error al crear carpeta", JOptionPane.ERROR_MESSAGE);				
				}
			}
		});
		btnCrear.setBounds(72, 145, 132, 23);
		contentPane.add(btnCrear);
		
		JButton btnRenombrar = new JButton("RENOMBRAR");
		btnRenombrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc1=new JFileChooser("/");
				fc1.setDialogTitle("Seleccione archivo o carpeta para renombrar");
				fc1.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fc1.setApproveButtonText("SELECCIONAR");
				int seleccion = fc1.showOpenDialog(ventanaGUI);
				if (seleccion == JFileChooser.APPROVE_OPTION)
				{
					JFileChooser fc2=new JFileChooser();
					fc2.setCurrentDirectory(new File(fc1.getSelectedFile().getParent())); //Abre segundo FileChooser en la carpeta padre del promero
					fc2.setDialogTitle("Seleccione nuevo nombre o ruta");
					fc2.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
					fc2.setApproveButtonText("RENOMBRAR");
					int seleccion2 = fc2.showSaveDialog(ventanaGUI);
					if (seleccion2 == JFileChooser.APPROVE_OPTION)
					{
						if (fc1.getSelectedFile().renameTo(fc2.getSelectedFile()))
							JOptionPane.showMessageDialog(ventanaGUI,"Se ha renombrado: "+fc1.getSelectedFile().getAbsolutePath()+ " --> "+fc2.getSelectedFile().getAbsolutePath());
						else
							JOptionPane.showMessageDialog(ventanaGUI, fc1.getSelectedFile().getAbsolutePath()+ " --> "+fc2.getSelectedFile().getAbsolutePath(), "Error al renombrar", JOptionPane.ERROR_MESSAGE);				
					}
				}
			}
		});
		btnRenombrar.setBounds(72, 84, 132, 23);
		contentPane.add(btnRenombrar);
		
		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc=new JFileChooser("/");
				fc.setDialogTitle("Seleccione archivo o carpeta para borrar");
				fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				fc.setApproveButtonText("BORRAR");
				int seleccion = fc.showOpenDialog(ventanaGUI);
				if (seleccion == JFileChooser.APPROVE_OPTION)
				{
					if (fc.getSelectedFile().delete())
						JOptionPane.showMessageDialog(ventanaGUI,"Se ha borrado: "+fc.getSelectedFile().getAbsolutePath());
					else
						JOptionPane.showMessageDialog(ventanaGUI, fc.getSelectedFile().getAbsolutePath(), "Error al borrar", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnBorrar.setBounds(72, 25, 132, 23);
		contentPane.add(btnBorrar);
	}
}
