package pdf1_soluciones;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import comun.Entrada;
import comun.Utilidades;

public class BuscarPorTamanioConsola { //ej1b

	public static void main(String[] args) {
		System.out.println("=== Buscar archivos por tamaño ===");
		System.out.print("Carpeta dónde buscar:");
		File ruta=new File(Entrada.cadena());
		System.out.print("Tamaño en bytes:");
		long tam=(long)Entrada.real();
		System.out.print("Mayores(+) o menores(-):");
		char mayMen=Entrada.caracter();
		System.out.print("Incluir ocultos (S/N) ?");
		char ocultos=Entrada.caracter();
		System.out.print("Incluir subcarpetas (S/N) ?");
		char subcarpetas=Entrada.caracter();
		
		try {
			ArrayList<File> res=Utilidades.buscarArchivosPorTamanio(ruta, tam, mayMen, ocultos=='S', subcarpetas=='S');	
			//Muestra resultado de la búsqueda
			for (int i = 0; i < res.size(); i++)
				System.out.println(Utilidades.mostrarInfoFileTamanio(res.get(i)));
			System.out.println(res.size()+" archivos encontrados.");
		} catch (Exception e) {
			Utilidades.notificaError(null, "Error al buscar", e, "Algún parámetro es incorrecto");
		} 
	}

}
