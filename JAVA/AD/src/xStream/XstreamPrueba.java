package xStream;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XstreamPrueba {

	public static void main(String[] args) {
		
		XStream xstream = new XStream(new DomDriver()); // does not require XPP3 library

		xstream.alias("person", Person.class);
		xstream.alias("phonenumber", PhoneNumber.class);
		
		Person joe = new Person("Joe", "Walnes");
		joe.setPhone(new PhoneNumber(123, "1234-456"));
		joe.setFax(new PhoneNumber(123, "9999-999"));
		
		Person pepe = new Person("Rloco", "Rlocura");
		pepe.setPhone(new PhoneNumber(322, "1434-456"));
		pepe.setFax(new PhoneNumber(345, "9000-999"));
		
		String xml = xstream.toXML(joe);
		System.out.println(xml+"\n");
		
		String xml2 = xstream.toXML(pepe);
		System.out.println(xml2);
	}

}
