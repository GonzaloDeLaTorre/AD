package pdf2;

public class Ej5_Persona {
	int edad; // >0
	String nombre;
	char sexo; // M-Masculino, F-Femenino
	
	public Ej5_Persona(int edad, String nombre, char sexo) {
		super();
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
	}

	public String toString() {
		return "Ej5_Persona [edad=" + edad + ", nombre=" + nombre + ", sexo=" + sexo + "]";
	}
	
}
