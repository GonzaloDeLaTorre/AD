package pdf2;

public class Ej6_Trabajo {
	int tipo;
	String descripcion;
	
	public Ej6_Trabajo(int tipo, String descripcion) {
		this.tipo = tipo;
		this.descripcion = descripcion;
	}
	
	public String toString() {
		return "Trabajo [tipo=" + tipo + ", descripcion=" + descripcion + "]";
	}
}
