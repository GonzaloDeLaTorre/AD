package pdf2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


import utilidades.Entrada;

public class Ej1 {

	public static void main(String[] args) throws IOException {
		System.out.print("Ruta del fichero: ");
		String ruta=Entrada.cadena(); // /media/Datos/coches.csv
		
		System.out.println("Contenido del fichero .csv:");
		abrir(ruta);

	}

	private static void abrir(String ruta) throws IOException {
		File ruta1=new File(ruta);
		if (ruta1.exists()) {
			leer(ruta1);
		} else {
			System.err.println("Error al abrir fichero.");
		}	
	}
	
	private static void leer(File ruta) throws IOException {
		BufferedReader bfr=new BufferedReader(new FileReader(ruta));
		String linea;
		String linea2;
		while ((linea=bfr.readLine())!=null) {
			if (linea.contains(";")) {
				linea2=linea.replace(";", "\t"); //   \t = tabulador
				System.out.println(linea2);
			} 
//			else {
//				System.out.println(linea);
//			}
		}
		bfr.close();
	}

}
