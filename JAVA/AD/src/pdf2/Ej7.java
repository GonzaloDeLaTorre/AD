package pdf2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.awt.event.ActionEvent;

public class Ej7 extends JFrame {

	private JPanel contentPane;
	private JButton btnSeleccionarTexto;
	private JTextField textField_CadenaAConvertir;
	File ruta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ej7 frame = new Ej7();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ej7() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 282);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnSeleccionarTexto = new JButton("Seleccionar Texto");
		btnSeleccionarTexto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccionarTexto();
			}

			private void seleccionarTexto() {
				JFileChooser fc=new JFileChooser("/media/Datos");
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				int opcion=fc.showOpenDialog(null);
				if (opcion==JFileChooser.APPROVE_OPTION) {
					ruta=fc.getSelectedFile();
				} else {
					JOptionPane.showMessageDialog(btnSeleccionarTexto, "Selecciona el archivo", "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnSeleccionarTexto.setBounds(133, 32, 182, 25);
		contentPane.add(btnSeleccionarTexto);
		
		textField_CadenaAConvertir = new JTextField();
		textField_CadenaAConvertir.setBounds(181, 106, 229, 18);
		contentPane.add(textField_CadenaAConvertir);
		textField_CadenaAConvertir.setColumns(10);
		
		JLabel lblCadenaAConvertir = new JLabel("Cadena a convertir");
		lblCadenaAConvertir.setBounds(24, 107, 157, 17);
		contentPane.add(lblCadenaAConvertir);
		
		JButton btnConvertir = new JButton("CONVERTIR");
		btnConvertir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					convertir();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			private void convertir() throws IOException {
				try {
					RandomAccessFile raf=new RandomAccessFile(ruta, "rw");
					String linea;
					String buscando=textField_CadenaAConvertir.getText();
					long ppiolinea=0;
					while ((linea=raf.readLine())!=null) {
						if (linea.indexOf(buscando)!=-1) {
							linea=linea.replace(buscando, buscando.toUpperCase());
							raf.seek(raf.getFilePointer()-(linea.length()+1));
							raf.writeBytes(linea+"\n");
						}
						ppiolinea=raf.getFilePointer();
					}
					raf.close();
					if (textField_CadenaAConvertir.getText().equals("")) {
						JOptionPane.showMessageDialog(btnConvertir, "Texto vacío. Introduce palabra.", "Atención", JOptionPane.ERROR_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(btnConvertir, "¡ Conversión correcta !", "Correcto", JOptionPane.INFORMATION_MESSAGE);
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(btnConvertir, "Fallo al convertir", "Error", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		btnConvertir.setBounds(133, 179, 182, 25);
		contentPane.add(btnConvertir);
	}
}
