package pdf2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Ej6 {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Ej6_Persona p1=new Ej6_Persona(20, "Julio Perez", 'M');
		p1.añadirTrabajo(new Ej6_Trabajo(1, "Albanil"));
		p1.añadirTrabajo(new Ej6_Trabajo(1, "Encofrador"));
		
		Ej6_Persona p2=new Ej6_Persona(21, "Ana Gutierrez", 'F');
		p2.añadirTrabajo(new Ej6_Trabajo(2, "Ingeniero"));
		
		Ej6_Persona p3=new Ej6_Persona(84, "Emilia Rivilla", 'F');
		p3.añadirTrabajo(new Ej6_Trabajo(2, "Profesora"));
		p3.añadirTrabajo(new Ej6_Trabajo(3, "Locutora de radio"));
		
		File ubi_txt=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Trabajo.txt");
		escribirTxt(ubi_txt, p1, p2, p3);

		File ubi_bin=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Trabajo.bin");
//		escribirBin(ubi_bin, p1, p2, p3);
		escribirTxt(ubi_bin, p1, p2, p3);
		
		File ubi_obj=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Trabajo.obj");
//		escribirObj(ubi_bin, ubi_obj);
		escribirTxt(ubi_obj, p1, p2, p3);
		
		File ubi_xml=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Trabajo.xml");
		//escribirXml(ubi_xml, ubi_bin);
		escribirTxt(ubi_xml, p1, p2, p3);
	}
	

	private static void escribirObj(File ubi_bin, File ubi_obj) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(ubi_bin));
		ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(ubi_obj));
		Object per;
		while ((per=ois.readObject())!=null) {
			oos.writeObject(per);
		}
		ois.close();
	}


//	private static void escribirBin(File ubi_bin, Ej6_Persona p1, Ej6_Persona p2, Ej6_Persona p3) throws IOException {
//		DataInputStream dis=new DataInputStream(new FileInputStream(ubi_bin));
//		DataOutputStream dos=new DataOutputStream(new FileOutputStream(ubi_bin));
//		int n;
//		while ((n=dis.read())>=0) {
//			dis.write(p1);
//		}
//		dis.close();
//		dos.close();
//	}
	
	private static void escribirTxt(File ubi_txt, Ej6_Persona p1, Ej6_Persona p2, Ej6_Persona p3) throws FileNotFoundException  {
		PrintWriter pw=new PrintWriter(ubi_txt);
		pw.println(p1);
		pw.println(p2);
		pw.println(p3);
		pw.close();
	}
}
