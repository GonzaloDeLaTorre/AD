package pdf2;

import java.util.ArrayList;

public class Ej6_Persona {
	int edad; // >0
	String nombre;
	ArrayList<Ej6_Trabajo> trabs; //Trabajos en su vida laboral (0..n)
	char sexo;
	
	public Ej6_Persona(int edad, String nombre, char sexo) {
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
		this.trabs=new ArrayList<Ej6_Trabajo>();
	}
	
	public String toString() {
		return "Persona [edad=" + edad + ", nombre=" + nombre + ", trabs="
		+ trabs + ", sexo=" + sexo + "]";
	}
	
	public void añadirTrabajo(Ej6_Trabajo t){
		trabs.add(t);
	}
}
