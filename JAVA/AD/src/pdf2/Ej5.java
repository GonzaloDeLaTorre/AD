package pdf2;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StreamCorruptedException;

public class Ej5 implements Serializable {

	public static void main(String[] args) throws IOException, ClassNotFoundException, StreamCorruptedException {
		
		Ej5_Persona p1=new Ej5_Persona(20, "Julio Perez", 'M');
		Ej5_Persona p2=new Ej5_Persona(21, "Ana Gutierrez", 'F');
		Ej5_Persona p3=new Ej5_Persona(84, "Emilia Rivilla", 'F');
		
		File ubi_txt=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Personas.txt");
		File ubi_bin=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Personas.bin");
		File ubi_obj=new File("/media/Datos/GONZALO/Acceso a Datos/JAVA/AD/Personas.obj");
		escribir(ubi_txt, p1, p2, p3);
		escribir(ubi_bin, p1, p2, p3);
		escribir(ubi_obj, p1, p2, p3);
		
		leerTxt(ubi_txt);
		System.out.println("");
		leerBin(ubi_bin);
		System.out.println("");
		leerObj(ubi_obj); /*NO LO MUESTRAAAA*/

	}

	private static void leerObj(File ubi_obj) throws IOException { /*NO LO MUESTRAAAA*/
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(ubi_obj));
//		Object per;
//		while ((per=ois.readObject())!=null) {
//			System.out.println(per);
//		}
		String linea;
		while ((linea=ois.readLine())!=null) {
			System.out.println(linea);
		}
		ois.close();
	}

	private static void leerBin(File ubi_bin) throws IOException {
		DataInputStream dis=new DataInputStream(new FileInputStream(ubi_bin));
		String linea;
		while ((linea=dis.readLine())!=null) {
			System.out.println(linea);
		}
		dis.close();
	}

	private static void leerTxt(File ubi_txt) throws IOException {
		BufferedReader bfr=new BufferedReader(new FileReader(ubi_txt));
		String linea;
		while ((linea=bfr.readLine())!=null) {
			System.out.println(linea);
		}
		bfr.close();
	}

	private static void escribir(File ubi_txt, Ej5_Persona p1, Ej5_Persona p2, Ej5_Persona p3) throws FileNotFoundException {
		PrintWriter pw=new PrintWriter(ubi_txt);
		pw.println(p1);
		pw.println(p2);
		pw.println(p3);
		pw.close();
	}

}
