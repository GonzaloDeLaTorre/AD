package sucio;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Sucio {

	public static void main(String[] args) throws IOException {
		String ubi="";
		leer(ubi);

	}

	private static void leer(String ubi) throws IOException {	
		BufferedReader bfr=new BufferedReader(new FileReader(new File(ubi)));
		String linea;
		while ((linea=bfr.readLine())!=null) {
			System.out.println(linea);
		}
		bfr.close();
	}
}
