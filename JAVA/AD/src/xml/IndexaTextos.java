package xml;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class IndexaTextos {

	public static void main(String[] args) {
		try {

			// Crear fichero
			String ruta = "/home/alumno/biblioteca/indice.xml";
			RandomAccessFile fichero = new RandomAccessFile(ruta, "rw");

			// Leer archivo
			String pirata = "Pirata";
			String rutaPirata = "/home/alumno/biblioteca/Pirata";
			RandomAccessFile ficheroPirata = new RandomAccessFile(rutaPirata, "r");
//			mostrarFichero(ficheroPirata);

			// Leer archivo2
			String quijote = "Quijote";
			String rutaQuijote = "/home/alumno/biblioteca/Quijote";
			RandomAccessFile ficheroQuijote = new RandomAccessFile(rutaQuijote, "r");
//			mostrarFichero(ficheroQuijote);

			// Leer archivo3
			String vida = "Vida";
			String rutaVida = "/home/alumno/biblioteca/Vida";
			RandomAccessFile ficheroVida = new RandomAccessFile(rutaVida, "r");
//			mostrarFichero(ficheroVida);

			// Creamos arbol DOM
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			// crea elemento raiz
			Element raiz = doc.createElement("biblioteca");
			doc.appendChild(raiz);

			
// CREAR HIJO 1
			// crea elemento hijo, aniade atributo, y lo cuelga de raiz
			Element hijo = doc.createElement("libro");
			hijo.setAttribute("titulo", pirata);
			hijo.setTextContent("\n	");
			raiz.appendChild(hijo);

			Element hijo2 = doc.createElement("linea");
			String linea = null;
			int i = 1;
			ficheroPirata.seek(0); // nos situamos al principio
			for (int j = 0; j < ficheroPirata.length()-1; j++) {
				while ((linea = ficheroPirata.readLine()) != null) {
					hijo2.setAttribute("num", "" + i);
					i++;
				}
				hijo2.setTextContent(j + "");
			}
			hijo.appendChild(hijo2);

			
// CREAR HIJO 2
			Element hijo11 = doc.createElement("libro");
			hijo11.setAttribute("titulo", quijote);
			hijo11.setTextContent("\n	");
			raiz.appendChild(hijo11);

			hijo2 = doc.createElement("linea");
			linea = null;
			i = 1;
			ficheroQuijote.seek(0); // nos situamos al principio
			for (int j = 0; j < ficheroQuijote.length()-1; j++) {
				while ((linea = ficheroQuijote.readLine()) != null) {
					hijo2.setAttribute("num", "" + i);
					i++;
				}
				hijo2.setTextContent(j + "");
			}
			hijo11.appendChild(hijo2);

			
// CREAR HIJO 3
			Element hijo12 = doc.createElement("libro");
			hijo12.setAttribute("titulo", vida);
			hijo12.setTextContent("\n	");
			raiz.appendChild(hijo12);

			hijo2 = doc.createElement("linea");
			linea = null;
			i = 1;
			ficheroVida.seek(0); // nos situamos al principio
			for (int j = 0; j < ficheroVida.length()-1; j++) {
				while ((linea = ficheroVida.readLine()) != null) {
					hijo2.setAttribute("num", "" + i);
					i++;
				}
				hijo2.setTextContent(j + "");
			}
			hijo12.appendChild(hijo2);

			
			
			// Escribe arbol DOM a fichero XML
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(ruta));
			transformer.transform(source, result);

		} catch (Exception e) {
			System.err.println("Fallo, ha saltado el catch.");
		}

	}

//	public static String mostrarFichero(RandomAccessFile ficheroPirata) {
//		String n = null;
//		try {
//			ficheroPirata.seek(0); // nos situamos al principio
//			while ((n = ficheroPirata.readLine()) != null) {
//				System.out.println(n); // se muestra en pantalla
//			}
//		} catch (EOFException e) {
//			System.out.println("Fin de fichero");
//		} catch (IOException ex) {
//			System.out.println(ex.getMessage());
//		}
//		return n;
//	}

}
