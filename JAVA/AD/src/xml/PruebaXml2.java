package xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class PruebaXml2 {

	
	public static void main(String[] args) {

		
		try {
//==============================================================================================			
//Crear XML nuevo, p. ej:
//		<raiz>
//		   <!--Esto es un comentario...-->
//		   <hijo name="valor">Texto dentro del elemento hijo.</hijo>
//		</raiz>		
//==============================================================================================
			//Creamos arbol DOM
            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            //crea elemento raiz
            Element raiz = doc.createElement("raiz");
            doc.appendChild(raiz);

            //crea comentario
            Comment comment = doc.createComment("Esto es un comentario...");
            raiz.appendChild(comment);

            //crea elemento hijo, aniade atributo, y lo cuelga de raiz
            Element hijo = doc.createElement("hijo");
            hijo.setAttribute("name", "valor");
            hijo.setTextContent("Texto dentro del elemento hijo.");
            raiz.appendChild(hijo);   
            
            NodeList lista=doc.getChildNodes();
            System.out.println("Hijos de /");
			for (int i = 0; i < lista.getLength(); i++) {
				Node h = lista.item(i);
				System.out.println("Nodo de tipo: "+h.getNodeType());
			}
            lista=raiz.getChildNodes();
            System.out.println("Hijos de ");
			for (int i = 0; i < lista.getLength(); i++) {
				Node h = lista.item(i);
				System.out.println("Nodo de tipo: "+h.getNodeType());
			}
            lista=hijo.getChildNodes();
            System.out.println("Hijos de ");
			for (int i = 0; i < lista.getLength(); i++) {
				Node h = lista.item(i);
				System.out.println("Nodo de tipo: "+h.getNodeType());
			}

			// Escribe arbol DOM a fichero XML
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("nuevo.xml"));
			transformer.transform(source, result);
			
			
//==============================================================================================
//CARGAR(parser), recorrer y modificar fichero XML EXISTENTE, ej. desayunos.xml
//==============================================================================================
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.parse("desayunos.xml"); //Crea erbol DOM a partir de archivo XML
			
			//Obtenemos el precio del segundo desayuno mediante XPATH y cambiamos el valor del atributo moneda
			Element des2 = (Element) (XPathFactory.newInstance().newXPath().evaluate("/menu_desayunos/desayuno[2]/precio", doc,XPathConstants.NODE));
			des2.setAttribute("moneda", "rublo");
			
			//Borramos el elemento "descripcion" del segundo desayuno aprovechando la busqueda Xpath anterior
			lista=des2.getParentNode().getChildNodes();
			for (int i = 0; i < lista.getLength(); i++) {
				Node h = lista.item(i);
				if (h.getNodeName().equals("descripcion"))
					des2.getParentNode().removeChild(h);//Se borra elemento desde nodo padre
			}

			//Obtenemos total calorias mediante XPATH  . ! Ojo NODESET para lista de nodos!			
			lista=(NodeList) (XPathFactory.newInstance().newXPath().evaluate("/menu_desayunos/desayuno/calorias", doc,XPathConstants.NODESET));
			int total=0;
			for (int i = 0; i < lista.getLength(); i++) {
				if (lista.item(i).getNodeType() == Node.ELEMENT_NODE)
					total += Integer.parseInt(lista.item(i).getTextContent()) ;
			}
			System.out.println("TOTAL CALORIAS MENU: "+total);
			
			
			// Escribe arbol DOM a fichero XML
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			source = new DOMSource(doc);
			result = new StreamResult(new File("desayunos.modificado.xml"));
			transformer.transform(source, result);

			System.out.println("Fin programa.");

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}