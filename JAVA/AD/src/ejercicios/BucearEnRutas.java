package ejercicios;

import java.io.File;

import utilidades.Entrada;

public class BucearEnRutas {

	public static void main(String[] args) {
		System.out.print("Ruta Carpeta: ");
		String rutaUsuario=Entrada.cadena();
		File ruta=new File(rutaUsuario);
		File [] l=ruta.listFiles();
		if (ruta.exists()) {
			if (!ruta.isDirectory()) {
				System.out.println("No existe ningún directorio en esta ruta.");
			} else {
				if (l!=null) {
					for (int i = 0; i < l.length; i++) {
						if (l[i].isDirectory()) {
							System.out.println(l[i].getName()+" - es Directorio.");
							listarCarpeta(l[i]);
						}
						if (l[i].isFile()) {
							System.out.println(l[i].getName()+" - es Fichero.");
						}
					}
				}
			}
		} else {
			System.out.println("No existe la ruta seleccionada.");
		}
	}
	//  /media/Datos/GONZALO/Inglés

	private static void listarCarpeta(File ruta) {
		File [] l=ruta.listFiles();
		if (l!=null) {
			for (int i = 0; i < l.length; i++) {
				if (l[i].isDirectory()) {
					System.out.println(l[i].getName()+" - es Directorio.");
					listarCarpeta(l[i]);
				}
				if (l[i].isFile()) {
					System.out.println(l[i].getName()+" - es Fichero.");
				}
			}
		}
	}
}
