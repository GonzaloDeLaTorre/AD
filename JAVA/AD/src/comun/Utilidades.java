package comun;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utilidades {
	public static String mostrarInfoFileTamanio(File f) {
		return f.getAbsolutePath()+" ("+f.length()+" bytes)";
	}

	public static String mostrarInfoFileNombre(File f) {
		return f.getAbsolutePath();
	}

	public static String mostrarInfoFileFechaHora(File f) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fecha=sdf.format(f.lastModified());
		return f.getAbsolutePath()+" ("+fecha+")";
	}
	
	public static void notificaError(JFrame padre, String titulo,	Exception e, String mensaje) {
		String contenido="";
		if (mensaje!=null)
			contenido+=mensaje; 
		if (e!=null&&mensaje!=null) //Escribe un separador entre la descripción de la excepción y el mensaje
			contenido+="\n----------------------------------------------------\n";
		if (e!=null)
			contenido+=e.getClass().getName()+"\n"+e.getMessage(); //Nombre de la excepcion y mensaje de la excep.
		JOptionPane.showMessageDialog(padre,contenido,titulo, JOptionPane.ERROR_MESSAGE);		
	}
	
	public boolean preguntaUsuario(JFrame padre, String titulo, String mensaje){
		///Mensaje de confirmacion. Devuelve true si el usuario pulsa SI
		return JOptionPane.showConfirmDialog(padre, mensaje,titulo,JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION;
	}



	public static ArrayList<File> buscarArchivosPorTamanio(File carpetaDondeBuscar,long tamEnBytes, char criterio, boolean incluirOcultos, boolean incluirSubcarpetas) {
		if (carpetaDondeBuscar==null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File no puede ser null");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException(carpetaDondeBuscar.getAbsolutePath()+" no es una carpeta");
		if (tamEnBytes<0L)
			throw new IllegalArgumentException("El tamaño no puede ser negativo: "+tamEnBytes);
		boolean mayores = true;
		switch (criterio) {
		case '+':
			mayores = true;
			break;
		case '-':
			mayores = false;
			break;
		default:
			throw new IllegalArgumentException("El argumento criterio debe ser un caracter '+' o '-'");
		}
		
		ArrayList<File> al = new ArrayList<File>();
		
		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista==null) //Error al listar carpeta: falta de permisos, etc
			return al; //Devuelve arraylist con 0 elementos
		
		for (int i = 0; i < lista.length; i++) {
			boolean incluir= !lista[i].isHidden() || lista[i].isHidden()&&incluirOcultos;//Ignoramos carpetas y ficheros ocultos
			if (incluir){
				if (lista[i].isFile()){
					if (mayores && lista[i].length()>=tamEnBytes || !mayores && lista[i].length()<=tamEnBytes)
						al.add(lista[i]);
				}
				if (lista[i].isDirectory() && incluirSubcarpetas){
					//Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
					ArrayList<File> al2=buscarArchivosPorTamanio(lista[i],tamEnBytes,criterio,incluirOcultos,incluirSubcarpetas);
					al.addAll(al2);
				}
			}
		}
		return al;
	}


	public static ArrayList<File> buscarArchivosPorNombre(File carpetaDondeBuscar, String patronNombre, boolean incluirOcultos, boolean incluirSubcarpetas){
		if (carpetaDondeBuscar==null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File no puede ser null");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException(carpetaDondeBuscar.getAbsolutePath()+" no es una carpeta");
	
		ArrayList<File> al = new ArrayList<File>();
		
		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista==null) //Error al listar carpeta: falta de permisos, etc
			return al; //Devuelve arraylist con 0 elementos
		
		for (int i = 0; i < lista.length; i++) {
			boolean incluir= !lista[i].isHidden() || lista[i].isHidden()&&incluirOcultos;//Ignoramos carpetas y ficheros ocultos
			if (incluir){
				if (lista[i].isFile()){
					if (lista[i].getName().matches(patronNombre))//Comprueba si el nombre del fichero casa con el patrón (expresión regular) recibido
						al.add(lista[i]);
				}
				if (lista[i].isDirectory() && incluirSubcarpetas){
					//Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
					ArrayList<File> al2=buscarArchivosPorNombre(lista[i],patronNombre,incluirOcultos,incluirSubcarpetas);
					al.addAll(al2);
				}
			}
		}
		return al;
	}
	
	public static ArrayList<File> buscarArchivosPorFechaModif(File carpetaDondeBuscar, long fecha, boolean antesDe, boolean incluirOcultos, boolean incluirSubcarpetas){
		if (carpetaDondeBuscar==null)
			throw new IllegalArgumentException("El primer argumento debe ser un objeto de la clase File no puede ser null");
		if (!carpetaDondeBuscar.isDirectory())
			throw new IllegalArgumentException(carpetaDondeBuscar.getAbsolutePath()+" no es una carpeta");
	
		ArrayList<File> al = new ArrayList<File>();
		
		File[] lista = carpetaDondeBuscar.listFiles();
		if (lista==null) //Error al listar carpeta: falta de permisos, etc
			return al; //Devuelve arraylist con 0 elementos
		
		for (int i = 0; i < lista.length; i++) {
			boolean incluir= !lista[i].isHidden() || lista[i].isHidden()&&incluirOcultos;//Ignoramos carpetas y ficheros ocultos
			if (incluir){
				if (lista[i].isFile()){
					if (antesDe && lista[i].lastModified()<=fecha || !antesDe && lista[i].lastModified()>=fecha)
						al.add(lista[i]);
				}
				if (lista[i].isDirectory() && incluirSubcarpetas){
					//Añadimos los ficheros que se encuentren en subcarpetas mediante recursividad
					ArrayList<File> al2=buscarArchivosPorFechaModif(lista[i],fecha,antesDe,incluirOcultos,incluirSubcarpetas);
					al.addAll(al2);
				}
					
			}
		}
		return al;
	}
}
