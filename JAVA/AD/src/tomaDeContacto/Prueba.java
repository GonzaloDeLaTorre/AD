package tomaDeContacto;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;

public class Prueba extends JFrame {

	private JPanel contentPane;
	private JButton btnAbrir;
	private JButton btnGuardar;
	private JTextArea textArea;
	File ruta;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Prueba frame = new Prueba();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Prueba() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textArea = new JTextArea();
		textArea.setBackground(Color.WHITE);
		textArea.setBounds(77, 28, 293, 138);
		contentPane.add(textArea);
		
		btnAbrir = new JButton("Abrir");
		btnAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					abrir();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});
		btnAbrir.setBounds(77, 206, 117, 25);
		contentPane.add(btnAbrir);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					guardar(ruta);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnGuardar.setBounds(252, 206, 117, 25);
		contentPane.add(btnGuardar);
	}
	
	private void abrir() throws IOException {
		JFileChooser fc=new JFileChooser("./"); // "/media/Datos"
		File ruta;
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int opcion=fc.showOpenDialog(null);
		if (opcion==JFileChooser.APPROVE_OPTION) {
			ruta=fc.getSelectedFile();		
			leer(ruta);
			guardar(ruta);
		} else {
			JOptionPane.showMessageDialog(btnAbrir, "Selecciona el archivo", "Error", JOptionPane.ERROR_MESSAGE);
		}	
	}

	private void leer(File ruta) throws IOException {
		BufferedReader bfr=new BufferedReader(new FileReader(ruta));
		String linea;
		while ((linea=bfr.readLine())!=null) {
			textArea.setText(linea);
		}
		bfr.close();
	}
	
	private void guardar(File ruta) throws FileNotFoundException {
		
		PrintWriter pw=new PrintWriter(new File(textArea.getSelectedText()));
		pw.println();
		pw.close();
	}
	//DUDAAAAA: Hay que guardar en el mismo archivo previamente abierto, o hay que seleccionar el archivo en el que guardar???
}
