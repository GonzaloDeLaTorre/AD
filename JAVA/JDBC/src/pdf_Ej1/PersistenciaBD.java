package pdf_Ej1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class PersistenciaBD  implements Persistencia {

	String tipoBD;
	static Connection con;
	
	public static Connection PersistenciaBD(String tipoBD,String IP, String usu, String pass, String bd) throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:"+tipoBD+"://" + IP + "/" + bd, usu, pass);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion",
					JOptionPane.ERROR_MESSAGE);
		}
		return con;	
	}

	@Override
	public void desconectar() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarPersona(String tabla, Persona p) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarPersona(String tabla, String email) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Persona consultarPersona(String tabla, String email) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

}
