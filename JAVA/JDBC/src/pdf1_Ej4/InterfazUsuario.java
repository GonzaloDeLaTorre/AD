package pdf1_Ej4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazUsuario extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuario frame = new InterfazUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton Boton_Alta = new JButton("ALTA");
		Boton_Alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				crearAlta();
			}
		});
		Boton_Alta.setBounds(63, 60, 117, 25);
		contentPane.add(Boton_Alta);
		
		JButton Boton_Ingreso = new JButton("INGRESO");
		Boton_Ingreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				realizarIngreso();
			}
		});
		Boton_Ingreso.setBounds(245, 60, 117, 25);
		contentPane.add(Boton_Ingreso);
		
		JButton Boton_Reintegro = new JButton("REINTEGRO");
		Boton_Reintegro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				realizarReintegro();
			}
		});
		Boton_Reintegro.setBounds(63, 155, 117, 25);
		contentPane.add(Boton_Reintegro);
		
		JButton Boton_Extracto = new JButton("EXTRACTO");
		Boton_Extracto.setBounds(245, 155, 117, 25);
		contentPane.add(Boton_Extracto);
	}

//	protected void realizarReintegro() { //REINTEGRO
//		// Consultar ID de la cuenta
//		String j = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
//		boolean cond = condicionId(j);
//		if (cond == false) {
//			// Buscar ID
//			// Si existe el ID
//			boolean existe = true;
//			if (existe) {
//				String j2 = JOptionPane.showInputDialog(this, "ID: "/*ID de la cuenta*/+"\nSaldo: "/*Sueldo*/+"\nImporte del ingreso: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
//				cond = condicionImporte(j2);
//				if (cond == false ) {
//					int res = Integer.valueOf(j2);	
//					if (!(res > 0) && saldo res) {
//						JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.", "Error", JOptionPane.ERROR_MESSAGE);
//					} else {
//						// Incrementar saldo de la cuenta
//					}
//				}
//			} else {
//				JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
//			}
//		}
//	}

	protected void realizarIngreso() { //REINTEGRO
		// Consultar ID de la cuenta
		String j = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
		boolean cond = condicionId(j);
		if (cond == false) {
			// Buscar ID
			// Si existe el ID
			boolean existe = true;
			if (existe) {
				String j2 = JOptionPane.showInputDialog(this, "ID: "/*ID de la cuenta*/+"\nSaldo: "/*Sueldo*/+"\nImporte del ingreso: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
				cond = condicionImporte(j2);
				if (cond == false ) {
					int res = Integer.valueOf(j2);	
					if (!(res > 0)) {
						JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						// Incrementar saldo de la cuenta
					}
				}
			} else {
				JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	

	protected void crearAlta() { //ALTA --> Crear ID
		String j = JOptionPane.showInputDialog(this, "Introduce el nuevo ID para la cuenta: ", "Alta de cuenta", JOptionPane.DEFAULT_OPTION);
		condicionId(j);
		// Crear alta
	}
	
	
	private boolean condicionImporte(String j2) {
		boolean res = false;
		if (!j2.matches("[0-9]{1,11}")) {
			JOptionPane.showMessageDialog(this, "El importe debe tener una longitud mínima de 1 caracter numérico y máxima de 11 caracteres numéricos.", "Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}

	
	private boolean condicionId(String j) {
		boolean res = false;
		if (!j.matches("[0-9]{11}")) {
			JOptionPane.showMessageDialog(this, "El ID debe tener una longitud de 11 caracteres numéricos.", "Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}
	
}
