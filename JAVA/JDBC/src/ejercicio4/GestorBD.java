package ejercicio4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GestorBD {

	Connection con; // Objeto conexion

	public GestorBD() throws SQLException {

		try {
			// Comprobamos el driver
			Class.forName("com.mysql.jdbc.Driver");

		} catch (ClassNotFoundException e) {
			utilidades.Utilidades.notificarError(null, "Error SQL", e, "no se encuentra el driver.\n");
		}
		// Conectamos a la BBDD mysql
		con = DriverManager.getConnection("jdbc:mysql://localhost/minibanco", "root", "manager");

	}

	/**
	 * Metodo para consultar cuenta.
	 * 
	 * @param id
	 *            --> Clave primaria de la tabla clientes
	 * @return --> Retorna un cliente con todos los datos
	 * @throws Exception
	 */
	public Cuenta consultarCuenta(int id) throws Exception {

		int saldo = 0;
		Cuenta c1 = null;

		String sqlBusqueda = "SELECT * FROM cuenta WHERE id=" + "'" + id + "'";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			
			id = rs.getInt("id");
			saldo = rs.getInt("saldo");
			c1 = new Cuenta(id,saldo);

		}

		return c1;
	}
	
	/**
	 * Metodo para crear las cuentas.
	 * El id entra como null, se rellena automaticamente en la BBDD al tener auto_increment
	 * @param c --> Objeto de tipo Cuenta
	 * @throws Exception
	 */
	public void crearCuenta(Cuenta c) throws Exception {

		Cuenta cuenta = consultarCuenta(c.getId());
		
		//Si la cuenta no existe, se crea
		if (cuenta == null) {
			String sqlInsert = "INSERT INTO cuenta VALUES (?,?)";
			PreparedStatement preparedStatement = con.prepareStatement(sqlInsert);
			preparedStatement.setInt(1, c.getId()); //
			preparedStatement.setInt(2, c.getSaldo());
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
		}

	}
	
	public void ingreso(int idCuenta,int ingreso) throws Exception {
		
		//Actualizamos la tabla cuenta
		
		//Consultamos por el id si la cuenta existe
		Cuenta cuenta = consultarCuenta(idCuenta);
		int saldoAnterior = cuenta.getSaldo();
		int saldoFinal = ingreso+saldoAnterior;
		
		String sqlCuenta = "UPDATE cuenta SET saldo=? WHERE id="+idCuenta;
		PreparedStatement ps = con.prepareStatement(sqlCuenta);
		ps.setInt(1, saldoFinal);
		ps.executeUpdate();
		ps.close();
		
		//Actualizamos la tabla ingresos
		
		//Orden de insert: id,f_h,importe,id_cuenta
		/*Date date = new Date();
		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		System.out.println("Hora y fecha: "+hourdateFormat.format(date));*/
		String sqlMovimientos = "INSERT INTO movimiento (f_h,importe,id_cuenta) VALUES (?,?,?)";
		
		PreparedStatement psMovimientos = con.prepareStatement(sqlMovimientos);

		psMovimientos.setDate(1, getCurrentDate());
		//psMovimientos.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now())); //la hora la pone a 00:00;00
		psMovimientos.setInt(2, ingreso); 
		psMovimientos.setInt(3, idCuenta);
		psMovimientos.executeUpdate();
		psMovimientos.close();
		
		
	}
	
	
	public void reintegro(int idCuenta,int ingreso) throws Exception {
	//Actualizamos la tabla cuenta
		
		//Consultamos por el id si la cuenta existe
		Cuenta cuenta = consultarCuenta(idCuenta);
		int saldoAnterior = cuenta.getSaldo();
		int saldoFinal = saldoAnterior-ingreso;
		
		String sqlCuenta = "UPDATE cuenta SET saldo=? WHERE id="+idCuenta;
		PreparedStatement ps = con.prepareStatement(sqlCuenta);
		ps.setInt(1, saldoFinal);
		ps.executeUpdate();
		ps.close();
		
		//Actualizamos la tabla ingresos
		
		//Orden de insert: id,f_h,importe,id_cuenta
		/*Date date = new Date();
		DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
		System.out.println("Hora y fecha: "+hourdateFormat.format(date));*/
		String sqlMovimientos = "INSERT INTO movimiento (f_h,importe,id_cuenta) VALUES (?,?,?)";
		
		PreparedStatement psMovimientos = con.prepareStatement(sqlMovimientos);

		psMovimientos.setDate(1, getCurrentDate());
		//psMovimientos.setDate(1, java.sql.Date.valueOf(java.time.LocalDate.now())); //la hora la pone a 00:00;00
		psMovimientos.setInt(2, -ingreso); 
		psMovimientos.setInt(3, idCuenta);
		psMovimientos.executeUpdate();
		psMovimientos.close();
	}
	
	private static java.sql.Date getCurrentDate() {
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Date(today.getTime());
	}
	
	
	public boolean consultarExistencia(int id) throws Exception {
		
		boolean ex = false;

		String sqlBusqueda = "SELECT * FROM cuenta";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			if (rs.getString(1).equals(String.valueOf(id))) {
				ex = true;
			}
		}
		rs.close();
		st.close();

		return ex;
	}
	
	public int consultarSaldo(int id) throws Exception {
		
		int saldo = 0;

		String sqlBusqueda = "SELECT * FROM cuenta";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlBusqueda);

		// Guardamos en un objeto de tipo cuenta todos los datos
		while (rs.next()) {
			if (rs.getString(1).equals(String.valueOf(id))) {
				saldo = Integer.valueOf(rs.getString(2));
			}
		}
		rs.close();
		st.close();

		return saldo;
	}
	
	
}
