package ejercicio4;

public class Movimiento {
	
	int id; //AutoIncrement y primaryKey
	String f_h; //Fecha y hora
	int importe;
	int id_cuenta; //Clave foranea , es la clave primaria de la tabla Cuenta

	//CONSTRUCTOR
	public Movimiento(int id, String f_h, int importe, int id_cuenta) {
		super();
		this.id = id;
		this.f_h = f_h;
		this.importe = importe;
		this.id_cuenta = id_cuenta;
	}
	//GETTERS AND SETTERS
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getF_h() {
		return f_h;
	}
	public void setF_h(String f_h) {
		this.f_h = f_h;
	}
	public int getImporte() {
		return importe;
	}
	public void setImporte(int importe) {
		this.importe = importe;
	}
	public int getId_cuenta() {
		return id_cuenta;
	}
	public void setId_cuenta(int id_cuenta) {
		this.id_cuenta = id_cuenta;
	}
	
	
	

}
