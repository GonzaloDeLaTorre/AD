package ejercicio4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class InterfazUsuario extends JFrame{

	private JPanel contentPane;
	
	GestorBD bd = null;

	/**
	 * Launch the application.
	 * @throws SQLException 
	 */
	
	public static void main(String[] args) throws SQLException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuario frame = new InterfazUsuario();
					frame.setVisible(true);					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

			GestorBD bd = new GestorBD();

	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuario() {
		setEnabled(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 442, 387);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton Boton_Alta = new JButton("ALTA");
		Boton_Alta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				crearAlta();
			}
		});
		Boton_Alta.setBounds(63, 60, 117, 25);
		contentPane.add(Boton_Alta);
		
		JButton Boton_Ingreso = new JButton("INGRESO");
		Boton_Ingreso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					realizarIngreso();
				} catch (SQLException e) {
					utilidades.Utilidades.notificarError(null, "Ingreso", e, "Error al realizar el ingreso: \n");
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Boton_Ingreso.setBounds(245, 60, 117, 25);
		contentPane.add(Boton_Ingreso);
		
		JButton Boton_Reintegro = new JButton("REINTEGRO");
		Boton_Reintegro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					realizarReintegro();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			
		});
		Boton_Reintegro.setBounds(63, 155, 117, 25);
		contentPane.add(Boton_Reintegro);
		
		JButton Boton_Extracto = new JButton("EXTRACTO");
		Boton_Extracto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					extracto();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		Boton_Extracto.setBounds(245, 155, 117, 25);
		contentPane.add(Boton_Extracto);
		
		JTextArea textArea_Movimientos = new JTextArea();
		textArea_Movimientos.setBounds(63, 223, 302, 103);
		contentPane.add(textArea_Movimientos);
	}
	

	protected void extracto() throws NumberFormatException, Exception {
		bd = new GestorBD();
		String id = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
		boolean cond = condicionId(id);
		if (cond == false) {
			// Buscar ID
			boolean ex = bd.consultarExistencia(Integer.valueOf(id));
			if (ex != false) {
				// Si existe el ID
				String fechaDesde = JOptionPane.showInputDialog(this, "ID: "+id+"\nFecha desde: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
				cond = condicionFecha(fechaDesde);
				if (cond == true) {
					String fechaHasta = JOptionPane.showInputDialog(this, "ID: "+id+"\nFecha desde: "+fechaDesde+"\nFecha hasta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
					cond = condicionFecha(fechaHasta);
					if (cond == true) {
						JOptionPane.showMessageDialog(this, "ID: "+id+"\nFecha desde: "+fechaDesde+"\nFecha hasta: "+fechaHasta, "Consulta de cuenta", JOptionPane.INFORMATION_MESSAGE);
//						mostrarMovimientosExtractos(id, fechaDesde, fechaHasta);
					} else {
						JOptionPane.showMessageDialog(this, "Fecha incorrecta.\nFormato: yyyy-MM-dd\nEjemplo:2019-12-25", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(this, "Fecha incorrecta.\nFormato: yyyy-MM-dd\nEjemplo:2019-12-25", "Error", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

//	private void mostrarMovimientosExtractos(String id, String fechaDesde, String fechaHasta) throws SQLException {
//		// FALTA METER FECHA EN EL STRING SQLBUSQUEDA Y HACER LA CONEXION PARA EL STATEMENT
//		
//		bd = new GestorBD();
//		String sqlBusqueda = "SELECT * FROM cuenta where id = '"+id+"', /*fechassss*/";
//		Statement st = bd.createStatement();
//		ResultSet rs = st.executeQuery(sqlBusqueda);
//
//		// Guardamos en un objeto de tipo cuenta todos los datos
//		while (rs.next()) {
//			if (rs.getString(1).equals(String.valueOf(id))) {
//				ex = true;
//			}
//		}
//		rs.close();
//		st.close();
//		
//	}

	private boolean condicionFecha(String fechaDesde) {
		boolean res = false;
		if (fechaDesde.matches("^\\d{4}-\\d{2}-\\d{2}$")) {
			res = true;
		}
		return res;
	}

	protected void realizarReintegro() throws NumberFormatException, Exception { //REINTEGRO
		bd = new GestorBD();
		// Consultar ID de la cuenta
		String id = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
		boolean cond = condicionId(id);
		if (cond == false) {
			// Buscar ID
			boolean ex = bd.consultarExistencia(Integer.valueOf(id));
			if (ex != false) {
				// Si existe el ID
				int saldo = bd.consultarSaldo(Integer.valueOf(id));
				String j2 = JOptionPane.showInputDialog(this, "ID: "+id+"\nSaldo: "+saldo+"\nImporte del reintegro: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
				cond = condicionImporte(j2);
				if (cond == false) {
					int res = Integer.valueOf(j2);	
					if (!(res > 0 && res < saldo)) { // COMPROBAR
						JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						bd.reintegro(Integer.valueOf(id), res);
					}
				}
			} else {
				JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	
	protected void realizarIngreso() throws Exception { //INGRESO
		bd = new GestorBD();
		// Consultar ID de la cuenta
		//j es el id de la cuenta
		
		String j = JOptionPane.showInputDialog(this, "ID de la cuenta: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
		int id = Integer.parseInt(j);
		boolean cond = condicionId(j);
		if (cond == false) {
			// Buscar ID
			// Si existe el ID
			boolean ex = bd.consultarExistencia(id);
			if (ex != false) {
				int saldo = bd.consultarSaldo(Integer.valueOf(id));
				String j2 = JOptionPane.showInputDialog(this, "ID: "+id+"\nSaldo: "+saldo+"\nImporte del ingreso: ", "Consulta de cuenta", JOptionPane.DEFAULT_OPTION);
				cond = condicionImporte(j2);
				if (cond == false ) {
					int res = Integer.valueOf(j2); //importe a ingresar
					
					if (!(res > 0)) {
						JOptionPane.showMessageDialog(this, "El importe del ingreso debe ser mayor a cero.", "Error", JOptionPane.ERROR_MESSAGE);
					} else {
						// Incrementar saldo de la cuenta	
						try {
							bd.ingreso(id, res);
						} catch (Exception e) {
							utilidades.Utilidades.notificarError(null, "Cuenta", e, "La cuenta con id: "+id+" no existe!\n");
							e.printStackTrace();
						}
					}
				}
			}
		} else {
			JOptionPane.showMessageDialog(this, "El ID no existe.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	

	protected void crearAlta() { //ALTA --> Crear ID
		//String j = JOptionPane.showInputDialog(this, "Introduce el nuevo ID para la cuenta: ", "Alta de cuenta", JOptionPane.DEFAULT_OPTION);
		//condicionId(j);
		// Crear alta
		int saldo = 0;
		Cuenta c = new Cuenta(saldo);
		try {
			bd = new GestorBD();
			bd.crearCuenta(c);
		} catch (Exception e) {
			System.out.println("Error crear cuenta");
			e.printStackTrace();
		}
		
	}
	
	
	private boolean condicionImporte(String j2) {
		boolean res = false;
		if (!j2.matches("[0-9]{1,11}")) {
			JOptionPane.showMessageDialog(this, "El importe debe tener una longitud mínima de 1 caracter numérico y máxima de 11 caracteres numéricos.", "Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}

	
	private boolean condicionId(String j) {
		boolean res = false;
		if (!j.matches("[0-9]{1,11}")) {
			JOptionPane.showMessageDialog(this, "El ID debe tener una longitud mínima de 1 caracter numérico y máxima de 11 caracteres numéricos.", "Error", JOptionPane.ERROR_MESSAGE);
			res = true;
		}
		return res;
	}
}
