package pdf1_Ej2_Metadatos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import pdf_Ej1.PersistenciaBD;

public class Metadatos_PruebaConexion {

	public static void main(String[] args) throws Exception {
		
		java.sql.Connection con = conectarBBDD();
		
		listarBBDD(con);
		
		listarTablaBBDD(con);

	}
	
	private static void listarTablaBBDD(Connection con) throws SQLException {
		
		String bbdd=""; //MAL
		ResultSet rs = con.getMetaData().getTables(bbdd, null, "%", null); //Introducir bbdd, posiblemente desde parametro

		while (rs.next()) {
			String catalogo = rs.getString(1);
			String tabla = rs.getString(3);
		    System.out.println("TABLA=" + catalogo + "." + tabla);
		}
	}

	private static void listarBBDD(Connection con) throws SQLException {
		ResultSet rs = con.getMetaData().getCatalogs();

		while (rs.next()) {
		    System.out.println("TABLE_CAT = " + rs.getString("TABLE_CAT") );
		}
	}

	private static Connection conectarBBDD() throws Exception {
		java.sql.Connection con = null;

		ArrayList<String> lineas = new ArrayList<>();
		String linea;
		File f = new File("CFG2.INI");
		BufferedReader br = new BufferedReader(new FileReader(f));
		while ((linea = br.readLine()) != null) {
			lineas.add(linea);
		}
		br.close();
		
		String IP = lineas.get(0);
		String usu = lineas.get(1);
		String pass = lineas.get(2);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://"+IP+"/", usu, pass);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion",
					JOptionPane.ERROR_MESSAGE);
		}
		return con;
	}



}
