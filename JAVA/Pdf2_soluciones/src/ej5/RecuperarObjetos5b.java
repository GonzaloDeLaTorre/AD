package ej5;

import java.io.*;



public class RecuperarObjetos5b {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		//Leemos de fichero de texto
		BufferedReader bfr=new BufferedReader(new FileReader(new File("Personas.txt")));
		Persona p1=leerDesdeArchivoTexto(bfr);
		Persona p2=leerDesdeArchivoTexto(bfr);
		Persona p3=leerDesdeArchivoTexto(bfr);
		bfr.close();
		System.out.println("=== Objetos en Personas.txt ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Leemos de fichero binario
		DataInputStream dis=new DataInputStream(new FileInputStream(new File("Personas.bin")));
		p1=leerDesdeArchivoBinario(dis);
		p2=leerDesdeArchivoBinario(dis);
		p3=leerDesdeArchivoBinario(dis);
		dis.close();
		System.out.println("=== Objetos en Personas.bin ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Leemos de fichero de objetos serializados
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(new File("Personas.obj")));
		p1=leerDesdeArchivoObjetos(ois);
		p2=leerDesdeArchivoObjetos(ois);
		p3=leerDesdeArchivoObjetos(ois);
		ois.close();
		System.out.println("=== Objetos en Personas.obj ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
	}

	private static Persona leerDesdeArchivoObjetos(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		
		try {
			return (Persona)ois.readObject();
		} catch (EOFException e) {
			return null;
		}
	}

	private static Persona leerDesdeArchivoBinario(DataInputStream dis) throws IOException {
		//Lee cada atributo con el m�todo que corresponde a su tipo y crea el objeto
		int edad;
		String nombre;
		char sexo;
		
		try {
			edad=dis.readInt();
		} catch (EOFException e) {
			return null;
		}
		try {
			nombre=dis.readUTF();
		} catch (EOFException e) {
			return null;
		}
		try {
			sexo=dis.readChar();
		} catch (EOFException e) {
			return null;
		}
		
		return new Persona(edad, nombre, sexo);
	}

	private static Persona leerDesdeArchivoTexto(BufferedReader bfr) throws IOException {
		//Lee 3 lineas de texto, una por cada atributo y crea el objeto
		
		//Leemos edad
		String linea;
		linea=bfr.readLine();
		if (linea==null)
			return null;
		int edad=Integer.valueOf(linea);
		
		//Leemos nombre
		linea=bfr.readLine();
		if (linea==null)
			return null;
		String nombre=linea;
		
		//Leemos sexo		
		linea=bfr.readLine();
		if (linea==null)
			return null;
		char sexo=linea.charAt(0);
		
		return new Persona(edad, nombre, sexo);
	}
}
