package ej5;

import java.io.Serializable;


public class Persona implements Serializable{
	int edad; // >0
	String nombre;
	char sexo; //M-Masculino, F-Femenino

	
	public Persona(int edad, String nombre, char sexo) {
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
	}

	public String toString() {
		return "Persona [edad=" + edad + ", nombre=" + nombre + ", sexo=" + sexo + "]";
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
}

