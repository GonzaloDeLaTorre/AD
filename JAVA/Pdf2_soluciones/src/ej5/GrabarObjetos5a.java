package ej5;

import java.io.*;

public class GrabarObjetos5a {

	public static void main(String[] args) throws IOException {
		Persona p1=new Persona(20,"Julio Perez",'M');
		Persona p2=new Persona(21,"Ana Gutierrez",'F');
		Persona p3=new Persona(84,"Emilia Rivilla",'F');

		//Grabamos en fichero de texto
		PrintWriter pw=new PrintWriter(new FileWriter(new File("Personas.txt")));
		grabaEnArchivoTexto(pw,p1);
		grabaEnArchivoTexto(pw,p2);
		grabaEnArchivoTexto(pw,p3);
		pw.close();

		//Grabamos en fichero binario
		DataOutputStream dos=new DataOutputStream(new FileOutputStream(new File("Personas.bin")));
		grabaEnArchivoBinario(dos,p1);
		grabaEnArchivoBinario(dos,p2);
		grabaEnArchivoBinario(dos,p3);
		dos.close();
		
		//Grabamos en fichero de objetos serializados
		ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(new File("Personas.obj")));
		grabaEnArchivoObjetos(oos,p1);
		grabaEnArchivoObjetos(oos,p2);
		grabaEnArchivoObjetos(oos,p3);
		oos.close();

		System.out.println("=== Objetos guardados en ficheros con diferente formato ===");
		
	}

	private static void grabaEnArchivoObjetos(ObjectOutputStream oos, Persona p) throws IOException {
		//El objeto completo se escribe serializado en el fichero
		oos.writeObject(p);
	}

	private static void grabaEnArchivoBinario(DataOutputStream dos, Persona p) throws IOException {
		// Cada atributo se graba en el archivo utilizando el m�todo que corresponde a su tipo
		dos.writeInt(p.getEdad());
		dos.writeUTF(p.getNombre());
		dos.writeChar(p.getSexo());		
	}

	private static void grabaEnArchivoTexto(PrintWriter pw, Persona p) {
		//Un objeto --> 3 lineas, cada atributo una linea del fichero de texto
		pw.println(p.getEdad());
		pw.println(p.getNombre());
		pw.println(p.getSexo());
	}

}
