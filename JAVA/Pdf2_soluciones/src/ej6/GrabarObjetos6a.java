package ej6;

import java.io.*;
import java.util.Iterator;

public class GrabarObjetos6a {

	public static void main(String[] args) throws IOException {
		Persona p1=new Persona(20,"Julio Perez",'M');
		p1.aniadirTrabajo(new Trabajo(1,"Albanil"));
		p1.aniadirTrabajo(new Trabajo(1,"Encofrador"));
		Persona p2=new Persona(21,"Ana Gutierrez",'F');
		p2.aniadirTrabajo(new Trabajo(2,"Ingeniero"));
		Persona p3=new Persona(84,"Emilia Rivilla",'F');
		p3.aniadirTrabajo(new Trabajo(2,"Profesora"));
		p3.aniadirTrabajo(new Trabajo(3,"Locutora de radio"));
		
		//Grabamos en fichero de texto
		PrintWriter pw=new PrintWriter(new FileWriter(new File("Personas.txt")));
		grabaEnArchivoTexto(pw,p1);
		grabaEnArchivoTexto(pw,p2);
		grabaEnArchivoTexto(pw,p3);
		pw.close();

		//Grabamos en fichero binario
		DataOutputStream dos=new DataOutputStream(new FileOutputStream(new File("Personas.bin")));
		grabaEnArchivoBinario(dos,p1);
		grabaEnArchivoBinario(dos,p2);
		grabaEnArchivoBinario(dos,p3);
		dos.close();
		
		//Grabamos en fichero de objetos serializados
		ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(new File("Personas.obj")));
		grabaEnArchivoObjetos(oos,p1);
		grabaEnArchivoObjetos(oos,p2);
		grabaEnArchivoObjetos(oos,p3);
		oos.close();

		//Grabamos en fichero de texto
		PrintWriter xml=new PrintWriter(new FileWriter(new File("Personas.xml")));
		//Escribimos cabecera y elemento raiz
		xml.println("<?xml version=\"1.0\"?>");
		xml.println("<personas>");
		grabaEnArchivoTextoXML(xml,p1);
		grabaEnArchivoTextoXML(xml,p2);
		grabaEnArchivoTextoXML(xml,p3);
		//Escribimos fin de elemento raiz
		xml.println("</personas>");
		xml.close();
		
		System.out.println("=== Objetos guardados en ficheros con diferente formato ===");
		
	}

	private static void grabaEnArchivoObjetos(ObjectOutputStream oos, Persona p) throws IOException {
		//El objeto completo se escribe serializado en el fichero
		oos.writeObject(p);
	}

	private static void grabaEnArchivoBinario(DataOutputStream dos, Persona p) throws IOException {
		// Cada atributo se graba en el archivo utilizando el m�todo que corresponde a su tipo
		dos.writeInt(p.getEdad());
		dos.writeUTF(p.getNombre());
		dos.writeChar(p.getSexo());
		//Como puede haber un numero variable de trabajos, grabamos el numero de trabajos(size() del ArrayList) en una l�nea
		//para saber cuantos trabajos hay, al leer el objeto desde archivo
		dos.writeInt(p.getTrabs().size());
		//Grabamos dos lineas por cada trabajo
		for (Trabajo t : p.getTrabs()) {
			dos.writeInt(t.getTipo());
			dos.writeUTF(t.getDescripcion());
		}
	}

	private static void grabaEnArchivoTexto(PrintWriter pw, Persona p) {
		//Cada atributo simple en una linea del fichero de texto
		pw.println(p.getEdad());
		pw.println(p.getNombre());
		pw.println(p.getSexo());
		//Como puede haber un numero variable de trabajos, grabamos el numero de trabajos(size() del ArrayList) en una l�nea
		//para saber cuantos trabajos hay, al leer el objeto desde archivo
		pw.println(p.getTrabs().size());
		//Grabamos dos lineas por cada trabajo
		for (Trabajo t : p.getTrabs()) {
			pw.println(t.getTipo());
			pw.println(t.getDescripcion());
		}
	}
	
	private static void grabaEnArchivoTextoXML(PrintWriter pw, Persona p) {
		//Cada atributo simple en una linea del fichero de texto
		pw.println("\t<persona>");
		pw.println("\t\t<edad>"+p.getEdad()+"</edad>");
		pw.println("\t\t<nombre>"+p.getNombre()+"</nombre>");
		for (Trabajo t : p.getTrabs()) {
			pw.println("\t\t<trabajo>");
			pw.println("\t\t\t<tipo>"+t.getTipo()+"</tipo>");
			pw.println("\t\t\t<descripcion>"+t.getDescripcion()+"</descripcion>");
			pw.println("\t\t</trabajo>");
		}
		pw.println("\t\t<sexo>"+p.getSexo()+"</sexo>");
		pw.println("\t</persona>");
	}

}
