package ej6;


import java.io.Serializable;
import java.util.ArrayList;

public class Persona implements Serializable{
	int edad; // >0
	String nombre;
	ArrayList<Trabajo> trabs; //Trabajos en su vida laboral (0..n)
	char sexo;
	
	public Persona(int edad, String nombre, char sexo) {
		this.edad = edad;
		this.nombre = nombre;
		this.sexo = sexo;
		this.trabs=new ArrayList<Trabajo>();
	}

	public String toString() {
		return "Persona [edad=" + edad + ", nombre=" + nombre + ", trabs="
				+ trabs + ", sexo=" + sexo + "]";
	}

	public void aniadirTrabajo(Trabajo t){
		trabs.add(t);
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public ArrayList<Trabajo> getTrabs() {
		return trabs;
	}
}

