package ej6;

import java.io.*;
import java.util.StringTokenizer;



public class RecuperarObjetos6b {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		//Leemos de fichero de texto
		BufferedReader bfr=new BufferedReader(new FileReader(new File("Personas.txt")));
		Persona p1=leerDesdeArchivoTexto(bfr);
		Persona p2=leerDesdeArchivoTexto(bfr);
		Persona p3=leerDesdeArchivoTexto(bfr);
		bfr.close();
		System.out.println("=== Objetos en Personas.txt ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Leemos de fichero binario
		DataInputStream dis=new DataInputStream(new FileInputStream(new File("Personas.bin")));
		p1=leerDesdeArchivoBinario(dis);
		p2=leerDesdeArchivoBinario(dis);
		p3=leerDesdeArchivoBinario(dis);
		dis.close();
		System.out.println("=== Objetos en Personas.bin ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Leemos de fichero de objetos serializados
		ObjectInputStream ois=new ObjectInputStream(new FileInputStream(new File("Personas.obj")));
		p1=leerDesdeArchivoObjetos(ois);
		p2=leerDesdeArchivoObjetos(ois);
		p3=leerDesdeArchivoObjetos(ois);
		ois.close();
		System.out.println("=== Objetos en Personas.obj ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Leemos de fichero XML
		BufferedReader xml=new BufferedReader(new FileReader(new File("Personas.xml")));
		//Saltamos las dos primeras lineas
		xml.readLine();
		xml.readLine();
		p1=leerDesdeArchivoTextoXML(xml);
		p2=leerDesdeArchivoTextoXML(xml);
		p3=leerDesdeArchivoTextoXML(xml);
		bfr.close();
		System.out.println("=== Objetos en Personas.xml ===");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
	}

	private static Persona leerDesdeArchivoObjetos(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		
		try {
			return (Persona)ois.readObject();
		} catch (EOFException e) {
			return null;
		}
	}

	private static Persona leerDesdeArchivoBinario(DataInputStream dis) throws IOException {
		int edad;
		String nombre;
		char sexo;
		int numTrabs, tipo;
		String descripcion;
		
		try {
			edad=dis.readInt();
		} catch (EOFException e) {
			return null;
		}
		try {
			nombre=dis.readUTF();
		} catch (EOFException e) {
			return null;
		}
		try {
			sexo=dis.readChar();
		} catch (EOFException e) {
			return null;
		}
		
		Persona p = new Persona(edad, nombre, sexo);
		
		//Leemos n�mero de trabajos
		try {
			numTrabs=dis.readInt();
		} catch (EOFException e) {
			return null;
		}
		
		//Se leen trabajos y se aniaden a la persona
		for (int i=1;i<=numTrabs;i++){
			//Leemos tipo
			try {
				tipo=dis.readInt();
			} catch (EOFException e) {
				return null;
			}
			//Leemos descripcion
			try {
				descripcion=dis.readUTF();
			} catch (EOFException e) {
				return null;
			}
			p.aniadirTrabajo(new Trabajo(tipo, descripcion));
		}
		
		return p;
	}

	private static Persona leerDesdeArchivoTexto(BufferedReader bfr) throws IOException {
		//Leemos edad
		String linea;
		linea=bfr.readLine();
		if (linea==null)
			return null;
		int edad=Integer.valueOf(linea);
		
		//Leemos nombre
		linea=bfr.readLine();
		if (linea==null)
			return null;
		String nombre=linea;
		
		//Leemos sexo		
		linea=bfr.readLine();
		if (linea==null)
			return null;
		char sexo=linea.charAt(0);
		
		Persona p = new Persona(edad, nombre, sexo);
		
		
		//Leemos n�mero de trabajos
		linea=bfr.readLine();
		if (linea==null)
			return null;
		int numTrabs=Integer.valueOf(linea);
		//Se leen trabajos y se aniaden a la persona
		for (int i=1;i<=numTrabs;i++){
			//Leemos tipo
			linea=bfr.readLine();
			if (linea==null)
				return null;
			int tipo=Integer.valueOf(linea);	
			//Leemos descripcion
			linea=bfr.readLine();
			if (linea==null)
				return null;
			String descripcion=linea;
			p.aniadirTrabajo(new Trabajo(tipo, descripcion));
		}
		
		return p;
	}


	private static Persona leerDesdeArchivoTextoXML(BufferedReader bfr) throws IOException {
		
		//Saltamos <persona>
		String linea=bfr.readLine();
		if (linea==null)
			return null;
		//Leemos edad
		linea=bfr.readLine();
		if (linea==null)
			return null;
		int edad=Integer.valueOf(obtenerValor("<edad>",linea));
		
		//Leemos nombre
		linea=bfr.readLine();
		if (linea==null)
			return null;
		String nombre=obtenerValor("<nombre>",linea);
		Persona p = new Persona(edad, nombre, ' ');
		
		//Leemos trabajos hasta que encontramos etiqueta <sexo>
		linea=bfr.readLine().trim();
		if (linea==null)
			return null;
		while (linea.startsWith("<trabajo>")){
			//Leemos tipo
			linea=bfr.readLine();
			if (linea==null)
				return null;
			int tipo=Integer.valueOf(obtenerValor("<tipo>",linea));
			//Leemos descripcion
			linea=bfr.readLine();
			if (linea==null)
				return null;
			String descripcion=obtenerValor("<descripcion>",linea);
			
			p.aniadirTrabajo(new Trabajo(tipo, descripcion));
			
			//Saltamos </trabajo>
			linea=bfr.readLine();
			if (linea==null)
				return null;
			//Leemos siguiente etiqueta trabajo/sexo
			linea=bfr.readLine().trim();
			if (linea==null)
				return null;
		}
		
		//Leemos sexo		
		char sexo=obtenerValor("<sexo>",linea).charAt(0);
		p.setSexo(sexo);
		
		//Saltamos </persona>
		linea=bfr.readLine();
		if (linea==null)
			return null;
		return p;
	}

	private static String obtenerValor(String etiqueta, String linea) {
		//Comprueba que linea comienza por etiqueta y extrae el valor <etiqueta>valor</etiqueta>
		String t=linea.trim();
		if (!t.startsWith(etiqueta))
			return null;
		StringTokenizer st=new StringTokenizer(t,"<>");
		st.nextToken();//Saltamos etiqueta
		if (!st.hasMoreTokens())
			return null;
		return st.nextToken();
	}
}
