package ej4;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class CopiaDeArchivos {

	public static void main(String[] args) {
		if (args.length!=2){
			System.out.println("Debe indicar dos argumentos: fichero_origen y fichero_destino");
			System.exit(-1);
		}
		File fo = new File(args[0]);
		if (!fo.exists() || !fo.isFile()) {
			System.out.println(fo.getAbsolutePath() + " no es un fichero.");
			System.exit(-1);
		}
		
		File fd = new File(args[1]);
		if (fd.exists()) {
			System.out.println(fd.getAbsolutePath()	+ " ya existe. Se sobreescribirá\nDesea continuar (S/N)?");
			Scanner s=new Scanner(System.in);
			String resp = s.nextLine();
			resp = resp.toUpperCase();
			if (!resp.equals("S"))
				System.exit(-1);
		}

		try {
			//Abrimos fichero origen
			DataInputStream dis=new DataInputStream(new FileInputStream(fo));
			//Creamos fichero destino
			DataOutputStream dos=new DataOutputStream(new FileOutputStream(fd));

			int b;
			//Leemos y escribimos byte a byte hasta encontrar EOF
			while ((b=dis.read()) != -1) {
				dos.write(b);
			}
			//Cerramos ambos ficheros
			dis.close();
			dos.close();
			System.out.println(fo+" --> "+fd);
			System.out.println("Fichero copiado correctamente.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
