package ej1;

import java.io.*;
import java.util.StringTokenizer;


public class LeerCSV {

	public static void main(String[] args) {
		try {
			//Abrimos fichero
			BufferedReader bfr=new BufferedReader(new FileReader(new File("Coches.csv")));
			String linea=null;
			//Leemos lineas de texto
			while ((linea=bfr.readLine())!=null) {
				//Obtiene los campos de linea (separados por ;) y los muestra
				StringTokenizer st=new StringTokenizer(linea,";");
				while (st.hasMoreTokens()){
					System.out.print(st.nextToken());
					//Muestra espacio detrás de cada campo o salto de línea si es el último campo
					if (st.hasMoreElements())
						System.out.print(" ");
					else
						System.out.print("\n");
				}
			}
			//Cerramos fichero
			bfr.close();
		} catch (FileNotFoundException e) {
			System.out.println("No se encuentra Coches.csv");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
