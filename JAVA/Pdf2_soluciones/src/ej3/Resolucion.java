package ej3;

public class Resolucion {
	short ancho;
	short alto;
	
	public short getAncho() {
		return ancho;
	}
	public void setAncho(short ancho) {
		this.ancho = ancho;
	}
	public short getAlto() {
		return alto;
	}
	public void setAlto(short alto) {
		this.alto = alto;
	}
}
