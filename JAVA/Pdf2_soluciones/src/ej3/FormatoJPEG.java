package ej3;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FormatoJPEG {

	public static void main(String[] args) throws IOException {
		JFileChooser fc=new JFileChooser("/");
		fc.setDialogTitle("Seleccione archivo JPEG");
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int seleccion = fc.showOpenDialog(null);
		if (seleccion == JFileChooser.APPROVE_OPTION)
		{
			Resolucion r=obtenerResolucionJPEG(fc.getSelectedFile());
			if (r!=null)
				JOptionPane.showMessageDialog(null,fc.getSelectedFile().getAbsolutePath()+"\n"+r.getAncho()+" x "+r.getAlto());
			else
				JOptionPane.showMessageDialog(null, "Este archivo no es JPEG", fc.getSelectedFile().getAbsolutePath(), JOptionPane.ERROR_MESSAGE);				
		}
	}

	private static Resolucion obtenerResolucionJPEG(File ruta) throws IOException {
		Resolucion r=null;
		
		DataInputStream dis=new DataInputStream(new FileInputStream(ruta));
		
		//Comprueba 2 primeros bytes corresponden con formato JPEG (marca SOI)
		if (dis.read()!=0xFF)
			return null;
		if (dis.read()!=0xD8)
			return null;
		
		//Leemos byte a byte buscanco marca SOF0, dos bytes consecutivos con valores: 0xFF0xC0
		int valor;
		while ((valor=dis.read())!=-1){
			if (valor==0xFF){
				valor=dis.read();
				if (valor==-1) //EOF
					break; //Salimos del bucle
				if (valor==0xC0){ //SOF0 encontrado
					dis.skipBytes(3); //Saltamos 3 bytes (length+Data precision)
					r=new Resolucion();
					r.setAlto(dis.readShort());
					r.setAncho(dis.readShort());
					break;
				}
			}
		}
		//Cerramos el fichero
		dis.close();
		return r;//Será null si no se ha localizado la marca SOF0
	}

}
