package HibernateTools;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JOptionPane;

public class GestorBBDD {

static Connection con;
	
	public static Connection conectarBBDD() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost/banco", "root", "manager");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion",
					JOptionPane.ERROR_MESSAGE);
		}
		return con;	
	}
	
}
