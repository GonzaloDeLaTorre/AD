package HibernateTools;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class MainPrincipal {

	public static void main(String[] args) throws Exception {
		
//		java.sql.Connection con = GestorBBDD.conectarBBDD();
//		
//		Cliente n = new Cliente();
//		
//		crearCuenta(n, con);
		
		
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		//Abrimos sesion para comprobar si existe en la bbdd el profesor
		Session session=sessionFactory.openSession();
		
		
		Sucursal s = new Sucursal("Alpedrete", "28430");
//		List<correoelectronico> correosElectronicos=new ArrayList<>();
//		correosElectronicos.add(new correoelectronico(3, "rosa@yahoo.com",n));
//		correosElectronicos.add(new correoelectronico(2, "rosa@hotmail.com",n));
//		correosElectronicos.add(new correoelectronico(1, "rosa@gmail.com",n));
//		
//		n.setCorreosElectronicos(correosElectronicos);
		 
		
		session.beginTransaction();
		
		session.save(s);
		
		session.getTransaction().commit();
		session.close();
		
	}

//	public static void crearCuenta(Cliente c, Connection con) throws Exception {
//		java.sql.Connection conexion = con;
//		
//		java.sql.PreparedStatement pst;
//		String insert="INSERT INTO cliente (id,nombre,f_nac,direccion,nif) VALUES (1,'Gonzalo','1996-12-05','Alpedrete','53748722E')";		
//		pst = conexion.prepareStatement(insert);
//		pst.executeUpdate();
//		pst.close();
//
//	}

}
