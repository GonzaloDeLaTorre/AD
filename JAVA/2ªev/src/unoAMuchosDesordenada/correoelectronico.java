package unoAMuchosDesordenada;

public class correoelectronico {

	 private int idCorreo;
	 private String direccioncorreo;
	 private profesor profesor;
	 
	 public correoelectronico() {
		 
	 }
	 
	 
	public correoelectronico(int idCorreo, String direccioncorreo, profesor profesor) {
		this.idCorreo = idCorreo;
		this.direccioncorreo = direccioncorreo;
		this.profesor = profesor;
	}


	public int getIdCorreo() {
		return idCorreo;
	}


	public void setIdCorreo(int idCorreo) {
		this.idCorreo = idCorreo;
	}


	public String getDireccioncorreo() {
		return direccioncorreo;
	}


	public void setDireccioncorreo(String direccioncorreo) {
		this.direccioncorreo = direccioncorreo;
	}


	public profesor getProfesor() {
		return profesor;
	}


	public void setProfesor(profesor profesor) {
		this.profesor = profesor;
	}
	
}
