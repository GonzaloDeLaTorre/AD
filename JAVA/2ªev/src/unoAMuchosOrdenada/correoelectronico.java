package unoAMuchosOrdenada;

import java.io.Serializable;

public class correoelectronico implements Serializable {

	 private int idCorreo;
     private String direccionCorreo;
     private profesor profesor;

     public correoelectronico() {
     }

     public correoelectronico(int idCorreo,String direccionCorreo,profesor profesor) {
    	 this.idCorreo=idCorreo;
         this.direccionCorreo=direccionCorreo;
         this.profesor=profesor;
     }

	public int getIdCorreo() {
		return idCorreo;
	}

	public void setIdCorreo(int idCorreo) {
		this.idCorreo = idCorreo;
	}

	public String getDireccionCorreo() {
		return direccionCorreo;
	}

	public void setDireccionCorreo(String direccionCorreo) {
		this.direccionCorreo = direccionCorreo;
	}

	public profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(profesor profesor) {
		this.profesor = profesor;
	}
     
}
