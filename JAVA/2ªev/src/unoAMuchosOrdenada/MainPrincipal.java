package unoAMuchosOrdenada;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class MainPrincipal {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory;
		Configuration configuration = new Configuration();
		configuration.configure();
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
				.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		//Abrimos sesion para comprobar si existe en la bbdd el profesor
		Session session=sessionFactory.openSession();
		
		
		profesor profesor=new profesor(10, "Gonzalo", "UnoAMuchos", "Ordenado");
		List<correoelectronico> correosElectronicos=new ArrayList<>();
		correosElectronicos.add(new correoelectronico(3, "rosa@yahoo.com",profesor));
		correosElectronicos.add(new correoelectronico(2, "rosa@hotmail.com",profesor));
		correosElectronicos.add(new correoelectronico(1, "rosa@gmail.com",profesor));
		
		profesor.setCorreosElectronicos(correosElectronicos);
		 
		
		session.beginTransaction();
		
		session.save(profesor);
		
		session.getTransaction().commit();
		session.close();

	}

}
