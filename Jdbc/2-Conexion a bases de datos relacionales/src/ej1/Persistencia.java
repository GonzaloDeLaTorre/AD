package ej1;

import java.util.ArrayList;

public interface Persistencia {
	 void desconectar() throws Exception;
	 ArrayList <Persona> listadoPersonas(String tabla, String orderBy) throws Exception;
	 void guardarPersona(String tabla, Persona p) throws Exception; //inserta o actualiza
	 void borrarPersona(String tabla, String email) throws Exception;
	 Persona consultarPersona(String tabla, String email) throws Exception;
}
