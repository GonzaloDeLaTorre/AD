package ej1;

public class Persona {
	private String nombre;
	private String cp;
	private String pais;
	private String email;
	public Persona(String nombre, String cp, String pais, String email) {
		super();
		this.nombre = nombre;
		this.cp = cp;
		this.pais = pais;
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
