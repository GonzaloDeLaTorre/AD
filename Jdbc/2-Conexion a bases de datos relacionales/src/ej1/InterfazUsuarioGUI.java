/* Mantenimiento(ABMC): Altas, Bajas, Modificaciones y Consultas de la tabla persona creada con SQL:
CREATE TABLE persona (
  nombre varchar(255),
  CP varchar(255),
  pais varchar(255),
  email varchar(255) primary key
)
*/

package ej1;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.TextArea;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import ej2.AccesoCatalogoGUI;

import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Font;

public class InterfazUsuarioGUI extends JFrame {
	// Modos del formulario
	final static int INICIAL = 0; // Formulario vacío
	final static int ALTA = 1; // El usuario esta dando de alta a una persona
	final static int BAJA_MODIF = 2; // El usuario esta viendo los datos de una persona y tiene opcion de modificar o dar de baja
	int modo = INICIAL;
	// Objeto para manejar la bases de datos
	static Persistencia bd;
	static String tabla; // Nombre de la tabla donde están los datos (se obtiene de CFG.INI)

	private JPanel contentPane;
	private JTextField textEmail;
	private JFrame ventanaPrincipal = this; // Almacenamos una referencia a la ventana que se crea
	private JTextField textNombre;
	private JTextField textCp;
	private JTextField textPais;
	private JTextField textPruebas;
	private JButton btnLimpiar;
	private JButton btnBorrar;
	private JButton btnGuardar;
	private JButton btnSinOrdenar;
	private JTextArea textAreaListado;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		// Carga archivo CFG.INI que está en el mismo paquete que InterfazUsuarioGUI
		String baseDatos = null;
		String tipoBD = null;
		String ip = null;
		String usuario = null;
		String passwd = null;
		try {
			BufferedReader bfr = new BufferedReader(new InputStreamReader(InterfazUsuarioGUI.class.getResourceAsStream("CFG.INI")));
			tabla = bfr.readLine();
			baseDatos = bfr.readLine();
			tipoBD = bfr.readLine();
			ip = bfr.readLine();
			usuario = bfr.readLine();
			passwd = bfr.readLine();
		bfr.close();
		} catch (Exception e) {
			notificarError(null, "Error al leer parámetros de CFG.INI", e, null);
			System.exit(-1);
		}
		// Conecta con base de datos. La base se datos se manejará a través del objeto bd.
		try {
			bd = new PersistenciaBD(tipoBD, ip, usuario, passwd, baseDatos);
		} catch (Exception e) {
			notificarError(null, "Error al conectar con base de datos", e, null);
			System.exit(-1);
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazUsuarioGUI frame = new InterfazUsuarioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

private static void notificarError(JFrame padre, String titulo, Exception e, String mensaje) {
	String contenido = "";
	if (mensaje != null)
		contenido += mensaje+"\n";
	//Muestra la excepción y su causas(excepciones anidadas)
	while (e != null) {
		contenido += "\n["+e.getClass().getName() + "] " + e.getMessage(); // Tipo y mensaje de la excepción
		e=(Exception)e.getCause();
	}
	JOptionPane.showMessageDialog(padre, contenido, titulo, JOptionPane.ERROR_MESSAGE);
}

	private boolean preguntaUsuario(JFrame padre, String titulo, String mensaje) {
		/// Mensaje de confirmacion. Devuelve true si el usuario pulsa SI
		return JOptionPane.showConfirmDialog(padre, mensaje, titulo,
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}

	/**
	 * Create the frame.
	 */
	public InterfazUsuarioGUI() {
		setTitle("Tabla datos personas (ABMC/CRUD)");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 915, 669);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDni = new JLabel("e-mail");
		lblDni.setBounds(85, 15, 46, 14);
		contentPane.add(lblDni);

		textEmail = new JTextField();
		textEmail.setBounds(167, 12, 209, 20);
		contentPane.add(textEmail);
		textEmail.setColumns(10);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consultar();
			}
		});
		btnConsultar.setBounds(393, 11, 89, 23);
		contentPane.add(btnConsultar);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(85, 54, 46, 14);
		contentPane.add(lblNombre);

		textNombre = new JTextField();
		textNombre.setEditable(false);
		textNombre.setColumns(10);
		textNombre.setBounds(167, 51, 209, 20);
		contentPane.add(textNombre);

		JLabel lblCp = new JLabel("CP");
		lblCp.setBounds(85, 90, 72, 14);
		contentPane.add(lblCp);

		textCp = new JTextField();
		textCp.setEditable(false);
		textCp.setColumns(10);
		textCp.setBounds(167, 87, 89, 20);
		contentPane.add(textCp);

		JLabel lblPais = new JLabel("País");
		lblPais.setBounds(85, 131, 46, 14);
		contentPane.add(lblPais);

		textPais = new JTextField();
		textPais.setEditable(false);
		textPais.setColumns(10);
		textPais.setBounds(167, 128, 209, 20);
		contentPane.add(textPais);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
		btnGuardar.setVisible(false);
		btnGuardar.setBounds(395, 90, 89, 23);
		contentPane.add(btnGuardar);

		btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				borrar();
			}
		});
		btnBorrar.setVisible(false);
		btnBorrar.setBounds(494, 90, 89, 23);
		contentPane.add(btnBorrar);

		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiar();
			}
		});
		btnLimpiar.setVisible(false);
		btnLimpiar.setBounds(593, 90, 89, 23);
		contentPane.add(btnLimpiar);

		JPanel panelListados = new JPanel();
		panelListados.setBackground(new Color(135, 206, 250));
		panelListados.setBounds(10, 170, 878, 461);
		contentPane.add(panelListados);
		panelListados.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Listados",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelListados.setLayout(null);

		btnSinOrdenar = new JButton("Sin Ordenar");
		btnSinOrdenar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar("");
			}
		});
		btnSinOrdenar.setBounds(10, 21, 104, 23);
		panelListados.add(btnSinOrdenar);

		JButton btnPorEmail = new JButton("Por e-mail");
		btnPorEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar("ORDER BY email");
			}
		});
		btnPorEmail.setBounds(135, 21, 97, 23);
		panelListados.add(btnPorEmail);

		JButton btnPorNombre = new JButton("Por nombre");
		btnPorNombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar("ORDER BY nombre");
			}
		});
		btnPorNombre.setBounds(266, 21, 104, 23);
		panelListados.add(btnPorNombre);

		JButton btnPorCp = new JButton("Por CP");
		btnPorCp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar("ORDER BY cp");
			}
		});
		btnPorCp.setBounds(393, 21, 97, 23);
		panelListados.add(btnPorCp);

		JButton btnPorPais = new JButton("Por país");
		btnPorPais.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listar("ORDER BY pais");
			}
		});
		btnPorPais.setBounds(517, 21, 97, 23);
		panelListados.add(btnPorPais);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 55, 858, 395);
		panelListados.add(scrollPane);

		textAreaListado = new JTextArea();
		textAreaListado.setEditable(false);
		textAreaListado.setFont(new Font("Courier New", Font.PLAIN, 13));
		scrollPane.setViewportView(textAreaListado);

	}

	protected void listar(String orderBy) {
		ArrayList<Persona> alPers=null;
		try {
			alPers=bd.listadoPersonas(tabla,orderBy);
		} catch (Exception e) {
			notificarError(ventanaPrincipal, "Error al obtener listado", e, null);
			return;
		}
		textAreaListado.setText("");
		String lineaListado=String.format("%-45s | %-12s | %-10s | %-1s","e-mail ("+alPers.size()+" registros)","Nombre","CP","País");
		textAreaListado.append(lineaListado+"\n");
		textAreaListado.append("---------------------------------------------------------------------------------------------------------"+"\n");
		for (Persona p:alPers) {
			lineaListado=String.format("%-45.45s | %-12.12s | %-10.10s | %-1s",p.getEmail(),p.getNombre(),p.getCp(),p.getPais());
			textAreaListado.append(lineaListado+"\n");
		}
		//Posiciona textArea en la primera linea
		textAreaListado.setSelectionStart(0);
		textAreaListado.setSelectionEnd(0); 
	}

	private void borrar() {
		String email = textEmail.getText();
		if (preguntaUsuario(ventanaPrincipal, "Borrar", "Desea dar de baja la persona con e-mail: " + email)) {
			try {
				bd.borrarPersona(tabla,email);
				JOptionPane.showMessageDialog(ventanaPrincipal, "Se ha dado de baja la persona con e-mail: " + email);
				limpiar();
			} catch (Exception e) {
				notificarError(ventanaPrincipal, "Error al borrar Persona", e, null);
			}
		}
	}

	private void guardar() {
		Persona p = personaDesdeFormulario();
		if (p != null) { // Se ha creado objeto persona (no hay errores)
			try {
					bd.guardarPersona(tabla, p);
					JOptionPane.showMessageDialog(ventanaPrincipal, "Datos guardados correctamente");
					modoBajaModificacion();
			} catch (Exception e) {
				notificarError(ventanaPrincipal, "Error al guardar persona", e, null);
			}
		}
	}

	private Persona personaDesdeFormulario() {
		// Valida los campos del formulario y crea un objeto persona
		String email = textEmail.getText().trim();
		if (email.length()==0) {
			notificarError(ventanaPrincipal, "e-mail incorrecto", null, "El e-mail no puede estar vacío");
			return null;
		}
		String nombre = textNombre.getText().trim();
		if (nombre.length() == 0) {
			notificarError(ventanaPrincipal, "Nombre incorrecto", null, "El nombre no puede estar vacío");
			return null;
		}
		String cp = textCp.getText().trim();
		if (cp.length() == 0) {
			notificarError(ventanaPrincipal, "CP incorrecto", null, "El CP no puede estar vacío");
			return null;
		}
		String pais = textPais.getText().trim();
		if (pais.length() == 0) {
			notificarError(ventanaPrincipal, "País incorrecto", null, "El país no puede estar vacío");
			return null;
		}
		
		Persona p=new Persona(nombre, cp, pais, email);
		return p;
	}

	private void limpiar() {
		modo = INICIAL;
		// Oculta botones y limpia cajas de texto
		btnGuardar.setVisible(false);
		btnBorrar.setVisible(false);
		btnLimpiar.setVisible(false);
		textEmail.setText("");
		textNombre.setText("");
		textCp.setText("");
		textPais.setText("");

		textEmail.setEditable(true);
		textNombre.setEditable(false);
		textCp.setEditable(false);
		textPais.setEditable(false);
	}

	private void consultar() {
		String email = textEmail.getText().trim();
		if (email.length()==0) {
			notificarError(ventanaPrincipal, "e-mail incorrecto", null, "El e-mail no puede estar vacío");
			return;
		}
		try {
			Persona p = bd.consultarPersona(tabla,email);
			if (p == null) {
				if (preguntaUsuario(ventanaPrincipal, "e-mail no existe", "Desea dar de alta el e-mail: " + email))
					modoAlta();
			} else {
				mostrarPersona(p);
				modoBajaModificacion();
			}
		} catch (Exception e) {
			notificarError(ventanaPrincipal, "Error al consultar Persona", e, null);
		}

	}

	private void modoBajaModificacion() {
		modo = BAJA_MODIF;
		btnGuardar.setVisible(true);
		btnBorrar.setVisible(true);
		btnLimpiar.setVisible(true);
		textEmail.setEditable(false);
		textNombre.setEditable(true);
		textCp.setEditable(true);
		textPais.setEditable(true);
	}

	private void modoAlta() {
		modo = ALTA;
		btnGuardar.setVisible(true);
		btnBorrar.setVisible(false);
		btnLimpiar.setVisible(true);
		textEmail.setEditable(false);
		textNombre.setEditable(true);
		textCp.setEditable(true);
		textPais.setEditable(true);
	}

	private void mostrarPersona(Persona p) {
		// Muestra un objeto Persona en los campos del formulario
		textEmail.setText(p.getEmail());
		textNombre.setText(p.getNombre());
		textCp.setText(p.getCp());
		textPais.setText(p.getPais());
	}
}
