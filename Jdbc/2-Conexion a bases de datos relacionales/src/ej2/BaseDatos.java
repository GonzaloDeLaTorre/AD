package ej2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class BaseDatos {

	private Connection con;

	// Conexion con MySQL
	public BaseDatos(String IP, String usuario, String passwd, String bd) throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
		String cadenaConexion = "jdbc:mysql://" + IP;
		if (bd != null) // Si bd es null no se selecciona niguna BD al conectarse.
			cadenaConexion += "/" + bd;
		con = DriverManager.getConnection(cadenaConexion, usuario, passwd);
	}

	public void desconectar() {

		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public ArrayList<String> listarBases() throws SQLException {

		ArrayList<String> l = new ArrayList<String>();
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = null;
		res = meta.getCatalogs(); // En MySQL las tablas se agrupan en Catalogs(un catálogo es un BD)
		while (res.next()) {
			l.add(res.getString("TABLE_CAT"));
		}
		res.close();
		return l;
	}

	public ArrayList<String> listarTablas(String base) throws SQLException {
		ArrayList<String> l = new ArrayList<String>();
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = null;
		res = meta.getTables(base, null, "%", null); // En MySQL las tablas se buscan en Catalogs

		while (res.next()) {
			if (res.getString("TABLE_TYPE").equals("TABLE"))
				l.add(res.getString("TABLE_NAME"));
		}
		res.close();
		return l;
	}

	public ArrayList<Columna> listarColumnas(String base, String tabla) throws SQLException {
		ArrayList<Columna> l = new ArrayList<Columna>();
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = null;
		res = meta.getColumns(base, null, tabla, "%"); // En MySQL las tablas se buscan en Catalogs
		while (res.next()) {
			l.add(new Columna(res.getString("COLUMN_NAME"),res.getString("TYPE_NAME")));
		}

		res.close();
		return l;
	}

	public boolean existeTabla(String base, String tabla) throws SQLException {
		ArrayList<String> lt = listarTablas(base);
		for (String t : lt)
			if (t.toLowerCase().equals(tabla.toLowerCase()))
				return true;
		return false;
	}

	public boolean existeBD(String base) throws SQLException {
		ArrayList<String> lbd = listarBases();
		for (String b : lbd)
			if (b.toLowerCase().equals(base.toLowerCase()))
				return true;
		return false;
	}

}