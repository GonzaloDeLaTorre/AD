package ej2;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.border.TitledBorder;
import javax.swing.JPasswordField;
import javax.swing.ImageIcon;

import java.awt.Toolkit;

import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import java.awt.Color;

public class AccesoCatalogoGUI extends JFrame {

	private JPanel contentPane;
	static BaseDatos bd;
	private JPanel panel;
	private JComboBox bases;
	private JComboBox tablas;
	private JComboBox columnas;

	/**
	 * Launch the application.
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws IOException, SQLException {
		//Carga archivo CFG.INI que está en el mismo paquete que AccesoCatalogoGUI
		BufferedReader bfr=new BufferedReader(new InputStreamReader(AccesoCatalogoGUI.class.getResourceAsStream("CFG.INI")));
		String ip=bfr.readLine();
		String usuario=bfr.readLine();
		String passwd=bfr.readLine();
		bfr.close();
		bd=new BaseDatos(ip, usuario, passwd, null);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccesoCatalogoGUI frame = new AccesoCatalogoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws SQLException 
	 */
	public AccesoCatalogoGUI() throws SQLException {
		setTitle("Acceso a los metadatos de MySQL");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 633, 459);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBackground(new Color(51, 204, 153));
		panel.setLayout(null);
		panel.setBounds(10, 24, 597, 386);
		contentPane.add(panel);

		bases = new JComboBox();
		bases.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				tablas.removeAllItems();
				ArrayList<String> lista;
				try {
					lista = bd.listarTablas((String) bases.getSelectedItem());
					for (String b : lista) {
						tablas.addItem(b);
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		bases.setBounds(10, 55, 156, 25);
		panel.add(bases);

		JLabel lblNewLabel = new JLabel("Bases de Datos");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(10, 34, 156, 14);
		panel.add(lblNewLabel);

		JLabel lblSeleccioneTabla = new JLabel("Tablas");
		lblSeleccioneTabla.setHorizontalAlignment(SwingConstants.CENTER);
		lblSeleccioneTabla.setBounds(178, 34, 156, 14);
		panel.add(lblSeleccioneTabla);

		tablas = new JComboBox();
		tablas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				columnas.removeAllItems();
				ArrayList<Columna> lista=null;
					try {
						lista = bd.listarColumnas((String) bases.getSelectedItem(),(String) tablas.getSelectedItem());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					for (Columna c : lista) {
						columnas.addItem(c);
					}
			}
		});
		tablas.setBounds(178, 55, 156, 25);
		panel.add(tablas);
		
		columnas = new JComboBox();
		columnas.setBounds(356, 55, 218, 25);
		panel.add(columnas);
		
		JLabel lblColumnas = new JLabel("Columnas");
		lblColumnas.setHorizontalAlignment(SwingConstants.CENTER);
		lblColumnas.setBounds(356, 34, 156, 14);
		panel.add(lblColumnas);
		
		ArrayList<String> lista = bd.listarBases();
		for (String b : lista) {
				bases.addItem(b);
		}
	}

	public JPanel getPanelExport() {
		return panel;
	}

	public JComboBox getBases() {
		return bases;
	}

	public JComboBox getTablas() {
		return tablas;
	}
}
