package ej2;

public class Columna {
	@Override
	public String toString() {
		return nombre + " (" + tipo + ")";
	}

	String nombre;
	String tipo; //VARCHAR, INTEGER, ....
	
	public Columna(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
